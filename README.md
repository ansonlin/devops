# Devops environment
此環境將會一鍵安裝Jenkins、Registry、Portainer、Gitlab、Zentao，以下為安裝步驟，有問題請回饋。



1.  Prepare Clean OS environment. 
Here recommand use  CentOS/RHEL minimal installation.


2.  Security Configuration.
$ sudo sed -i 's/enforcing/disabled/'  /etc/selinux/config 
$ sudo firewall-cmd --add-port tcp/80 --permanent;sudo firewall-cmd --reload
$ sudo reboot


3.  Install Docker Engine & Docker-Compose
$ sudo yum install docker -y
$ sudo usermod -aG docker [your-login-user-name]
$ sudo curl -L https://github.com/docker/compose/releases/download/1.20.1/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose


4. Install Service
$ sudo sh install.sh 


5. Check Service is alive
  (1) Jenkins    http://jenkins.192.168.1.114.nip.io/ 
  (2) Registry   http://registry.192.168.1.114.nip.io/ 
  (3) Portainer  http://portainer.192.168.1.114.nip.io/ 
  (4) Gitlab     http://gitlab.192.168.1.114.nip.io/ 
  (5) Zentao     http://zentao.192.168.1.114.nip.io/ 




>
> ## CNCF landsape
>
![prometheus](https://landscape.cncf.io/images/landscape.png)