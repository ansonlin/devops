

1.架構說明
1台Loadbalancer 
3台K8s 節點(4 vCPU、16 GB RAM)


2.事先準備
(1)OS install
CentOS 7.5 minimal install 
$sudo yum update

(2)System Configuration
$ sudo hostnamectl set-hostname rke1

###關閉防火牆(測試階段)
$ sudo systemctl disable firewalld && sudo systemctl stop firewalld

###關閉selinux --> 
$ sudo sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
$ sudo setenforce 0


###關閉 swap -->
$ sudo swapoff -a
$ sudo sysctl -w vm.swappiness=0
$ sudo sed -i 's/.*swap.*/#&/' /etc/fstab


$ sudo vi /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1 
net.bridge.bridge-nf-call-iptables = 1
vm.swappiness=0

$sudo vi /etc/hosts
10.40.141.101   rke1
10.40.141.102   rke2
10.40.141.103   rke3
  
3.必須工具  
(1) Docker Engine(1.13.1)
$ sudo yum install docker -y
$ sudo systemctl start docker && sudo systemctl enable docker
$ sudo vi /etc/docker/daemon.json
  {
  "group": "dockerroot",
  "registry-mirrors": ["http://mirror.10.40.40.158.nip.io"]
  }
$ sudo systemctl stop docker && sudo systemctl start docker  
$ sudo adduser rkeadmin
$ sudo passwd  rkeadmin  
$ sudo usermod -aG dockerroot rkeadmin

(2) Required Tools - RKE
https://github.com/rancher/rke/releases

$ chmod +x rke_linux-amd64
$ mv rke_linux-amd64 rke
$ sudo mv rke /usr/bin


(3) Kubectl

#cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

#yum install -y kubectl-1.11.4-0



(4)Helm
$ curl https://raw.githubusercontent.com/helm/helm/master/scripts/get > get_helm.sh
$ chmod 700 get_helm.sh
$ ./get_helm.sh
$ sudo mv /usr/local/bin/helm /usr/bin/ && sudo mv /usr/local/bin/tiller /usr/bin/



==============以上重複建立三台VM==============


5.透過RKE安裝kubernetes

(1)安裝LoadBalancer
$ vi nginx.conf

worker_processes 4;
worker_rlimit_nofile 40000;

events {
    worker_connections 8192;
}

http {
    server {
        listen         80;
        return 301 https://$host$request_uri;
    }
}

stream {
    upstream rancher_servers {
        least_conn;
        server <IP_NODE_1>:443 max_fails=3 fail_timeout=5s;
        server <IP_NODE_2>:443 max_fails=3 fail_timeout=5s;
        server <IP_NODE_3>:443 max_fails=3 fail_timeout=5s;
    }
    server {
        listen     443;
        proxy_pass rancher_servers;
    }
}

$ docker run -d --restart=unless-stopped \
  -p 80:80 -p 443:443 \
  -v '$PWD'/nginx.conf:/etc/nginx/nginx.conf \
  nginx:1.14


(2)###passwordless ssh(以連線到rke1主機為例)
$ ssh-keygen -t rsa
$ ssh rkeadmin@rke1 mkdir -p .ssh
$ cat .ssh/id_rsa.pub | ssh rkeadmin@rke1 'cat >> .ssh/authorized_keys'
$ ssh rkeadmin@rke1 "chmod 700 .ssh; chmod 640 .ssh/authorized_keys"
$ ssh rkeadmin@rke1




(3)
###建立設定檔
$ sudo vi  rancher-cluster.yml

nodes:
  - address: 10.40.141.111
    user: rkeadmin
    role: [controlplane,worker,etcd]
  - address: 10.40.141.112
    user: rkeadmin
    role: [controlplane,worker,etcd]
  - address: 10.40.141.113
    user: rkeadmin
    role: [controlplane,worker,etcd]

services:
  etcd:
    snapshot: true
    creation: 6h
    retention: 24h

###透過設定檔安裝
$ sudo rke up  --config  ./rancher-cluster.yml	
.......
.......
......

$ mkdir -p $HOME/.kube
$ cp kube_config_rancher-cluster.yml $HOME/.kube/config
$ chown $(id -u):$(id -g) $HOME/.kube/config

$ kubectl get nodes
$ kubectl get pods --all-namespaces


(4) helm
$ helm version
$ kubectl -n kube-system create serviceaccount tiller
$ kubectl create clusterrolebinding tiller \
  --clusterrole cluster-admin \
  --serviceaccount=kube-system:tiller
$ helm init --service-account tiller

## Test
$ kubectl -n kube-system  rollout status deploy/tiller-deploy
$ helm version



6.安裝Rancher


$ helm repo add rancher-stable https://releases.rancher.com/server-charts/stable
$ helm repo list

$ helm install stable/cert-manager --name cert-manager --namespace kube-system
$ kubectl -n kube-system rollout status deploy/cert-manager


$ helm install rancher-stable/rancher --name rancher --namespace cattle-system --set hostname=rancher.10.40.141.111.nip.io
$ kubectl -n cattle-system rollout status deploy/rancher







