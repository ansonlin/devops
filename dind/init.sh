#!/bin/sh

sleep 3

### initial docker swarm ###
echo "init docker swarm"
docker swarm init

echo "check swarm is active"
docker node ls

echo ""
echo ""

### Deploy Portainer Service ###
echo "Deploy Portainer Service"
echo ""
echo ""
echo "wait a moment to Download image"

docker pull portainer/portainer:1.16.5


echo ""
echo "Deploy Service"
docker service create --name portainer --publish 9000:9000 --replicas=1  --constraint 'node.role == manager' --mount type=bind,src=/var/run/docker.sock,dst=/var/run/docker.sock  --mount type=volume,src=portainer-data,dst=/data portainer/portainer:1.16.5 -H unix:///var/run/docker.sock


echo ""
echo ""
sleep 3
echo "check service is working"
docker service ls 
