package com.digiwin;

import com.digiwin.core.DeployProcess;
import com.digiwin.utils.CommonParam;
import com.digiwin.utils.LoggerUtil;

public class DeployCenter {
	/**
	 * 模式-執行更新
	 */
	private static final String CMD_MODE_RUNAPP = "runApp";
	private static final String CMD_MODE_RUNSYS = "runSys";
	/**
	 * 參數-部署區編號
	 */
	private static final String CMD_ARG_AREA = "--Area";
	private static final String CMD_ARG_MANUAL = "-m";

	public static void main(String[] args) {
		//args = new String[] { "runApp", "--Area", "dev" };
		try {
			Integer result = 0;
			if (args.length > 0) {
				switch (args[0]) {
				case CMD_MODE_RUNAPP:// 部署更新模式
					result = runApp(args);
					break;
				case CMD_MODE_RUNSYS:// 部署更新模式
					result = runSys(args);
					break;
				default:
					result = CommonParam.DEPLOY_FAILED;
					logger("請輸入執行模式");
					break;
				}
			} else {
				result = CommonParam.DEPLOY_FAILED;
				logger("請輸入執行模式");
			}
			System.exit(result);
		} catch (Exception e) {
			e.printStackTrace();
			logger(e.getMessage());
			System.exit(1);
		}
	}

	/**
	 * 執行應用程式部署
	 * 
	 * @param args
	 * @throws Exception
	 */
	private static Integer runApp(String[] args) throws Exception {
		String areaID = null;
		boolean isAuto = true;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals(CMD_ARG_AREA)) {
				areaID = args[i + 1];
			}
		}
		DeployProcess dp = new DeployProcess(areaID, CommonParam.TYPE_APPLICATION_SERVICE, isAuto);
		return dp.start();
	}

	/**
	 * 執行管理系統部署
	 * 
	 * @param args
	 * @throws Exception
	 */
	private static Integer runSys(String[] args) throws Exception {
		String areaID = null;
		boolean isAuto = false;
		for (int i = 0; i < args.length; i++) {
			if (args[i].equals(CMD_ARG_AREA)) {
				areaID = args[i + 1];
			}
		}
		DeployProcess dp = new DeployProcess(areaID, CommonParam.TYPE_SYSTEM_SERVICE, isAuto);
		return dp.start();
	}

	/**
	 * 寫入 log
	 * 
	 * @param msg
	 */
	private static void logger(String msg) {
		// System.out.println(msg);
		LoggerUtil.info(msg);
	}
}
