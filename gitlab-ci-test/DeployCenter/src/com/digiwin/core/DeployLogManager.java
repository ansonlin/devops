package com.digiwin.core;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.digiwin.model.DeployLog;
import com.digiwin.utils.DeployDao;
import com.digiwin.utils.StringUtil;

public class DeployLogManager {
	private Map<String, DeployLog> serviceLogMap = null;
	private Map<String, Object> areaConfig = null;
	private static final String STATUS_UPGRADE_NOT_EXECUTED = "1";
	private static final String STATUS_UPGRADE_SUCCESS = "2";
	private static final String STATUS_UPGRADE_FAILED = "3";
	private String updateTime = null;

	public DeployLogManager(Map<String, Object> areaConfig) {
		this.areaConfig = areaConfig;
		init();
	}

	/**
	 * 初始化部署 log
	 */
	private void init() {
		serviceLogMap = new HashMap<String, DeployLog>();
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
		updateTime = format.format(new Date());
	}

	/**
	 * 加入服務
	 * 
	 * @param serviceName
	 */
	public void addService(String serviceName) {
		DeployLog serviceLog = new DeployLog();
		serviceLog.setAreaID(areaConfig.get("AreaID").toString());
		serviceLog.setServiceName(serviceName);
		serviceLogMap.put(serviceName, serviceLog);
	}

	/**
	 * 紀錄部署版本
	 * 
	 * @param serviceName
	 * @param serviceVersion
	 */
	public void setServiceVersion(String serviceName, String serviceVersion) {
		DeployLog serviceLog = serviceLogMap.get(serviceName);
		serviceLog.setServiceVersion(serviceVersion);
	}

	/**
	 * 取得本次部署版本
	 * 
	 * @param serviceName
	 * @return
	 */
	public String getServiceVersion(String serviceName) {
		DeployLog serviceLog = serviceLogMap.get(serviceName);
		return serviceLog.getServiceVersion();
	}

	/**
	 * 紀錄部署使用的 image
	 * 
	 * @param serviceName
	 * @param image
	 */
	public void setImage(String serviceName, String image) {
		DeployLog serviceLog = serviceLogMap.get(serviceName);
		serviceLog.setImage(image);
	}

	/**
	 * 沒有版更
	 * 
	 * @param serviceName
	 */
	public void upgradeNotExecuted(String serviceName) {
		DeployLog serviceLog = serviceLogMap.get(serviceName);
		serviceLog.setServiceStatus(STATUS_UPGRADE_NOT_EXECUTED);
	}

	/**
	 * 部署成功
	 * 
	 * @param serviceName
	 */
	public void upgradeSuccess(String serviceName) {
		DeployLog serviceLog = serviceLogMap.get(serviceName);
		serviceLog.setServiceStatus(STATUS_UPGRADE_SUCCESS);
	}

	/**
	 * 部署失敗
	 * 
	 * @param serviceName
	 */
	public void upgradeFailed(String serviceName) {
		DeployLog serviceLog = serviceLogMap.get(serviceName);
		serviceLog.setServiceStatus(STATUS_UPGRADE_FAILED);
	}

	/**
	 * 紀錄部署失敗訊息
	 * 
	 * @param serviceName
	 * @param error
	 */
	public void setErrorMessage(String serviceName, String error) {
		DeployLog serviceLog = serviceLogMap.get(serviceName);
		serviceLog.setError(error);
		upgradeFailed(serviceName);
	}

	/**
	 * 紀錄服務類型
	 * 
	 * @param serviceName
	 * @param serviceType
	 */
	public void setServiceType(String serviceName, String serviceType) {
		DeployLog serviceLog = serviceLogMap.get(serviceName);
		serviceLog.setServiceType(serviceType);
	}

	/**
	 * 設定部署方式
	 * 
	 * @param serviceName
	 * @param isAuto
	 */
	public void setDeployBy(String serviceName, boolean isAuto) {
		if (isAuto) {
			setDeployByAuto(serviceName);
		} else {
			setDeployByManual(serviceName);
		}
	}

	/**
	 * 自動部署
	 * 
	 * @param serviceName
	 */
	public void setDeployByAuto(String serviceName) {
		DeployLog serviceLog = serviceLogMap.get(serviceName);
		serviceLog.setDeployBy("Auto");
	}

	/**
	 * 手動部署
	 * 
	 * @param serviceName
	 */
	public void setDeployByManual(String serviceName) {
		DeployLog serviceLog = serviceLogMap.get(serviceName);
		serviceLog.setDeployBy("Manual");
	}

	/**
	 * 部署指令
	 * 
	 * @param serviceName
	 * @param cmd
	 */
	public void setDeployCmd(String serviceName, String cmd) {
		DeployLog serviceLog = serviceLogMap.get(serviceName);
		serviceLog.setDeployCmd(cmd);
	}

	/**
	 * 判斷是否有部署紀錄
	 * 
	 * @return
	 */
	public boolean hasDeployLog() {
		if (serviceLogMap != null && serviceLogMap.size() > 0) {
			return true;
		} else {
			return false;
		}
	}

	public void insertLog() {
		if (serviceLogMap != null && serviceLogMap.size() > 0) {
			for (String serviceName : serviceLogMap.keySet()) {
				DeployLog serviceLog = serviceLogMap.get(serviceName);
				if (StringUtil.isEmptyOrSpace(serviceLog.getAreaID()) || StringUtil.isEmptyOrSpace(serviceLog.getServiceName()) || StringUtil.isEmptyOrSpace(serviceLog.getServiceVersion()) || StringUtil.isEmptyOrSpace(updateTime)) {
					continue;
				}
				Map<String, Object> logMap = new HashMap<String, Object>();
				logMap.put("AreaID", serviceLog.getAreaID());
				logMap.put("ServiceName", serviceLog.getServiceName());
				logMap.put("ServiceVersion", serviceLog.getServiceVersion());
				logMap.put("UpdateTime", updateTime);
				logMap.put("ServiceStatus", serviceLog.getServiceStatus());
				logMap.put("ErrorLog", serviceLog.getError());
				logMap.put("ServiceType", serviceLog.getServiceType());
				logMap.put("DeployBy", serviceLog.getDeployBy());
				logMap.put("DeployCmd", serviceLog.getDeployCmd());
				String sql = DeployDao.getMapToInsertSql("versionFromDeploy", logMap);
				DeployDao.executeSql(sql);
				if (serviceLog.getServiceStatus().equals(STATUS_UPGRADE_SUCCESS)) {
					DeployDao.updateAreaServiceImage(serviceLog.getAreaID(), serviceLog.getServiceName(), serviceLog.getImage());
				}
			}
		}
	}
}
