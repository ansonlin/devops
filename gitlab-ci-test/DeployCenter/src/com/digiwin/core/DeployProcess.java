package com.digiwin.core;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digiwin.model.DeployData;
import com.digiwin.utils.CommonParam;
import com.digiwin.utils.DeployDao;
import com.digiwin.utils.LoggerUtil;
import com.digiwin.utils.StringUtil;

public class DeployProcess {

	private String areaID;
	private String serviceType;
	private boolean isAuto;
	private Map<String, Object> areaConfig = null;
	private ServiceManager serviceManager = null;
	private NginxManager nginxManager = null;
	private Map<String, Object> serviceMap = null;
	private DeployLogManager deployLogManager = null;
	private static final String newline = "\r\n";

	public DeployProcess(String areaID, String serviceType, boolean isAuto) throws Exception {
		this.areaID = areaID;
		this.serviceType = serviceType;
		this.isAuto = isAuto;
		initArea();
	}

	/**
	 * 初始化部署區配置
	 * 
	 * @throws Exception
	 */
	private void initArea() throws Exception {
		List<Map<String, Object>> results = DeployDao.getAreaConfig(areaID);
		if (results == null || results.size() == 0) {
			throw new Exception("找不到部署區 " + areaID + " 資料");
		}
		areaConfig = results.get(0);
		deployLogManager = new DeployLogManager(areaConfig);
	}

	/**
	 * 開始部署
	 * 
	 * @throws Exception
	 */
	public Integer start() {
		Integer result = 0;
		try {
			if ("Y".equals(areaConfig.get("IsRelease"))) {
				result = startFromRelease();
			} else if ("N".equals(areaConfig.get("IsRelease"))) {
				result = startFromParentHost();
			} else {
				logger("config 參數設定錯誤 : IsRelease");
				result = CommonParam.DEPLOY_FAILED;
			}
			if (deployLogManager.hasDeployLog()) {
				loggerStep("寫入版本更新紀錄");
				deployLogManager.insertLog();
			}
		} catch (Exception e) {
			result = CommonParam.DEPLOY_FAILED;
			e.printStackTrace();
			logger(e.getMessage());
		} finally {
			return result;
		}
	}

	/**
	 * nginx 管理器初始化
	 */
	private void initNginx() {
		if (nginxManager == null) {
			nginxManager = new NginxManager(areaConfig);
		}
	}

	/**
	 * 開始發佈部署流程，更新來源為發佈檔案
	 * 
	 * @throws Exception
	 */
	public Integer startFromRelease() throws Exception {
		// 取出部署區目前要部署的所有服務
		serviceMap = getAreaService();
		List<DeployData> deployDataList = new ArrayList<DeployData>();
		loggerSplit("發佈版本檢查開始");
		loggerStep("取得發佈檔案位置");
		// 取出部署區的發佈檔案資料夾
		List<String> releasePath = getAreaReleasePath();
		if (releasePath == null || releasePath.size() == 0) {
			throw new Exception("找不到發佈檔案位置。");
		}
		// 服務管理
		serviceManager = new ServiceManager(areaConfig);
		// 發佈檔案管理
		ReleaseManager releaseManager = new ReleaseManager(areaConfig, releasePath);
		// 存入需要被更新的服務
		List<String> upgradeServiceList = new ArrayList<String>();
		Map<String, Object> releaseFiles = releaseManager.getReleaseFiles(serviceMap);
		for (String serviceName : serviceMap.keySet()) {
			loggerStep("服務 " + serviceName + " 版本檢查");
			if (!releaseFiles.containsKey(serviceName)) {
				loggerRuntime("服務 " + serviceName + " 沒有發佈檔案，不需更新。");
				continue;
			}
			// 紀錄本次部署服務
			deployLogManager.addService(serviceName);
			// 部署服務類型
			deployLogManager.setServiceType(serviceName, serviceType);
			// 部署方式
			deployLogManager.setDeployBy(serviceName, isAuto);

			// 當前服務資料
			Map<String, Object> serviceData = (Map<String, Object>) serviceMap.get(serviceName);
			Map<String, String> releaseFile = (Map<String, String>) releaseFiles.get(serviceName);
			// 紀錄發布檔案版本
			loggerRuntime("服務 " + serviceName + " 最新發佈版本 : " + releaseFile.get("version"));
			deployLogManager.setServiceVersion(serviceName, releaseFile.get("version"));
			// 檢查當前版本與 tar 是否需要更新
			loggerRuntime("取得 " + serviceName + " 當前運行資訊");
			Map<String, String> runningService = serviceManager.getServiceByName(serviceName);
			if (runningService == null) {
				// 有發佈檔案，但是沒有運行，視為更新
				// 紀錄部署資訊
				upgradeServiceList.add(serviceName);
				DeployData deployData = new DeployData();
				// 服務名稱
				deployData.setServiceName(serviceName);
				// 更新版 image
				deployData.setUpgradeImage(releaseFile.get("imageName"));
				// 首次安裝
				deployData.setFirstDeploy(true);
				// 是否灰度部署
				if (!StringUtil.isEmptyOrSpace(serviceData.get("GrayscaleDeploy")) && Integer.valueOf(serviceData.get("GrayscaleDeploy").toString()) > 0) {
					deployData.setGrayscale(true);
				}
				// 預設部署類型
				deployData.setDefaultType(serviceData.get("DefaultType") == null ? "" : serviceData.get("DefaultType").toString());
				// 部署順序
				deployData.setQueue(Integer.valueOf(serviceData.get("Queue").toString()));
				deployDataList.add(deployData);
				loggerRuntime("服務 " + serviceName + " 首次安裝。");
			} else if (needUpdate(releaseFile.get("version"), runningService.get("version"))) {
				// 紀錄部署資訊
				upgradeServiceList.add(serviceName);
				DeployData deployData = new DeployData();
				// 服務名稱
				deployData.setServiceName(serviceName);
				// 部署服務名稱
				deployData.setDeployName(runningService.get("NAME"));
				// 運行中 image
				deployData.setRunImage(runningService.get("IMAGE"));
				// 更新版 image
				deployData.setUpgradeImage(releaseFile.get("imageName"));
				// 更新
				deployData.setFirstDeploy(false);
				// 是否灰度部署
				if (!StringUtil.isEmptyOrSpace(serviceData.get("GrayscaleDeploy")) && Integer.valueOf(serviceData.get("Queue").toString()) > 0) {
					deployData.setGrayscale(true);
				}
				// 預設部署類型
				deployData.setDefaultType(serviceData.get("DefaultType") == null ? "" : serviceData.get("DefaultType").toString());
				// 部署順序
				deployData.setQueue(Integer.valueOf(serviceData.get("Queue").toString()));
				deployDataList.add(deployData);
				loggerRuntime("服務 " + serviceName + " 準備更新。");
			} else {
				loggerRuntime("服務 " + serviceName + " 版本相同不需更新。");
				deployLogManager.upgradeNotExecuted(serviceName);
			}
		}
		loggerSplit("發佈版本檢查結束");
		if (upgradeServiceList.size() == 0) {
			return CommonParam.DEPLOY_SUCCESS; // 結束更新
		}
		loggerSplit("服務相依檢查開始");
		// 檢查部署服務相依是否符合要求
		loggerStep("服務相依檢查");
		List<Map<String, String>> runningServiceList = serviceManager.getServiceList();
		if (!checkDependService(upgradeServiceList, runningServiceList)) {
			return CommonParam.DEPLOY_FAILED; // 相依檢查失敗結束更新
		}
		// 檢查灰度部署相依
		loggerStep("服務灰度部署需求檢查");
		checkDependOnGrayscale(deployDataList, runningServiceList);
		loggerSplit("服務相依檢查結束");
		// push
		loggerSplit("Image Push 到 Registry 開始");
		loggerStep("Push Image 到 Registry");
		for (String upgradeService : upgradeServiceList) {
			Map<String, String> fileMap = (Map<String, String>) releaseFiles.get(upgradeService);
			releaseManager.pushToRegistry(fileMap);
		}
		loggerSplit("Image Push 到 Registry 結束");

		// 整理好更新資料，開始部署
		if (deployDataList.size() > 0) {
			// 資料夾排序由 1 開始
			Collections.sort(deployDataList, new Comparator<DeployData>() {
				@Override
				public int compare(DeployData o1, DeployData o2) {
					// 排序
					return o1.getQueue() - o2.getQueue();
				}
			});
			loggerStep("本次版更清單 ");
			// 本次版更
			for (DeployData data : deployDataList) {
				loggerRuntime("服務 : " + data.getServiceName());
				loggerRuntime("版本 : " + data.getUpgradeImage());
				if (data.isFirstDeploy()) {
					loggerRuntime("安裝 : 首次安裝");
				} else if (data.isSyncDeploy()) {
					loggerRuntime("安裝 : 同步啟動");
				} else {
					loggerRuntime("安裝 : 版本更新");
				}
				if (data.isGrayscale()) {
					loggerRuntime("更新方式 : 灰度部署");
				} else {
					loggerRuntime("更新方式 : 單獨部署");
				}
				loggerRuntime("");
			}
			// 開始部署
			return deploy(deployDataList);
		}
		return CommonParam.DEPLOY_SUCCESS;
	}

	/**
	 * 開始部署更新流程，更新來源為上層主機
	 * 
	 * @throws Exception
	 */
	private Integer startFromParentHost() throws Exception {
		// 取出部署區目前要部署的服務
		serviceMap = getAreaService();
		List<DeployData> deployDataList = new ArrayList<DeployData>();
		String basisHostID = areaConfig.get("VersionBasisHostID").toString();
		List<Map<String, Object>> result = DeployDao.getHostArea(basisHostID);
		if (result == null || result.size() == 0) {
			throw new Exception("找不到更新依據設定。");
		}
		// 服務管理
		serviceManager = new ServiceManager(areaConfig);
		// 取得上層主機所屬區域編號
		String basisAreaID = result.get(0).get("AreaID").toString();
		// 取出來源服務
		Map<String, Object> sourceServiceMap = getAreaAllService(basisAreaID);
		List<String> upgradeServiceList = new ArrayList<String>();
		loggerSplit("版本檢查開始");
		// 檢查版本對應是否需要更新
		for (String serviceName : serviceMap.keySet()) {
			loggerStep("服務 " + serviceName + " 版本檢查");
			Map<String, Object> serviceData = (Map<String, Object>) serviceMap.get(serviceName);
			Map<String, Object> sourceServiceData = (Map<String, Object>) sourceServiceMap.get(serviceName);
			// 紀錄本次部署服務
			deployLogManager.addService(serviceName);
			// 部署服務類型
			deployLogManager.setServiceType(serviceName, serviceType);
			// 部署方式
			deployLogManager.setDeployBy(serviceName, isAuto);

			// 比對版本是否與來源部署區相同，不同則需要更新
			if (StringUtil.isEmptyOrSpace(serviceData.get("Image")) && !StringUtil.isEmptyOrSpace(sourceServiceData.get("Image"))) {
				// 當前部署區沒有部署該服務，且來源有部署時，視為更新
				loggerRuntime("服務 " + serviceName + " 準備更新。");
				upgradeServiceList.add(serviceName);
				// 取得來源部署區 image
				DeployData deployData = new DeployData();
				// 服務名稱
				deployData.setServiceName(serviceName);
				// 更新版 image
				deployData.setUpgradeImage(sourceServiceData.get("Image").toString());
				// 首次安裝
				deployData.setFirstDeploy(true);
				// 是否灰度部署
				if (!StringUtil.isEmptyOrSpace(serviceData.get("GrayscaleDeploy")) && Integer.valueOf(serviceData.get("GrayscaleDeploy").toString()) > 0) {
					deployData.setGrayscale(true);
				}
				// 預設部署類型
				deployData.setDefaultType(serviceData.get("DefaultType") == null ? "" : serviceData.get("DefaultType").toString());
				// 部署順序
				deployData.setQueue(Integer.valueOf(serviceData.get("Queue").toString()));
				deployDataList.add(deployData);
				loggerRuntime("服務 " + serviceName + " 首次安裝。");

			} else if (!StringUtil.isEmptyOrSpace(serviceData.get("Image")) && !serviceData.get("Image").equals(sourceServiceData.get("Image"))) {
				Map<String, String> runningService = serviceManager.getServiceByName(serviceName);
				// 當前部署區版本與來源部署區版本不一致時，視為更新
				// 紀錄部署資訊
				upgradeServiceList.add(serviceName);
				DeployData deployData = new DeployData();
				// 服務名稱
				deployData.setServiceName(serviceName);
				// 部署服務名稱
				deployData.setDeployName(runningService.get("NAME"));
				// 運行中 image
				deployData.setRunImage(runningService.get("IMAGE"));
				// 更新版 image
				deployData.setUpgradeImage(sourceServiceData.get("Image").toString());
				// 更新
				deployData.setFirstDeploy(false);
				// 是否灰度部署
				if (!StringUtil.isEmptyOrSpace(serviceData.get("GrayscaleDeploy")) && Integer.valueOf(serviceData.get("Queue").toString()) > 0) {
					deployData.setGrayscale(true);
				}
				// 預設部署類型
				deployData.setDefaultType(serviceData.get("DefaultType") == null ? "" : serviceData.get("DefaultType").toString());
				// 部署順序
				deployData.setQueue(Integer.valueOf(serviceData.get("Queue").toString()));
				deployDataList.add(deployData);
				loggerRuntime("服務 " + serviceName + " 準備更新。");
			} else {
				// 版本一致，不更新
				loggerRuntime("服務 " + serviceName + " 版本相同不需更新。");
				deployLogManager.upgradeNotExecuted(serviceName);
			}
		}
		loggerSplit("版本檢查結束");
		// 檢查部署服務相依是否符合要求
		loggerSplit("服務相依檢查開始");
		loggerStep("服務相依檢查");
		List<Map<String, String>> runningServiceList = serviceManager.getServiceList();
		if (!checkDependService(upgradeServiceList, runningServiceList)) {
			return CommonParam.DEPLOY_FAILED; // 相依檢查失敗
		}
		// 檢查灰度部署相依
		loggerStep("服務灰度部署需求檢查");
		checkDependOnGrayscale(deployDataList, runningServiceList);
		loggerSplit("服務相依檢查結束");
		// 整理好更新資料，開始部署
		if (deployDataList.size() > 0) {
			// 資料夾排序由 1 開始
			Collections.sort(deployDataList, new Comparator<DeployData>() {
				@Override
				public int compare(DeployData o1, DeployData o2) {
					// 排序
					return o1.getQueue() - o2.getQueue();
				}
			});
			loggerStep("本次版更清單 ");
			// 本次版更
			for (DeployData data : deployDataList) {
				loggerRuntime("服務 : " + data.getServiceName());
				loggerRuntime("版本 : " + data.getUpgradeImage());
				if (data.isFirstDeploy()) {
					loggerRuntime("安裝 : 首次安裝");
				} else if (data.isSyncDeploy()) {
					loggerRuntime("安裝 : 同步啟動");
				} else {
					loggerRuntime("安裝 : 版本更新");
				}
				if (data.isGrayscale()) {
					loggerRuntime("更新方式 : 灰度部署");
				} else {
					loggerRuntime("更新方式 : 單獨部署");
				}
				loggerRuntime("");
			}
			return deploy(deployDataList);
		}
		return CommonParam.DEPLOY_SUCCESS;
	}

	/**
	 * 執行部署
	 * 
	 * @param upgradeData
	 * @throws Exception
	 */
	private Integer deploy(List<DeployData> deployDataList) throws Exception {
		boolean hasGrayscale = false;
		boolean needReloadNginx = false;
		boolean needSwitch = false;
		List<DeployData> grayscalService = new ArrayList<DeployData>();
		// 根據更新資料，開始部署
		for (DeployData deployData : deployDataList) {
			loggerSplit("服務 " + deployData.getServiceName() + "部署開始");
			loggerStep("部署 " + deployData.getServiceName());
			if (deployData.isGrayscale()) {
				initNginx();
				// 執行灰度部署
				if (deployServiceByGrayscale(deployData)) {
					hasGrayscale = true; // 有灰度部署
					if (deployData.isUpgradeNginx()) { // 有更新 nginx
						needReloadNginx = true;
					}
					if (!deployData.isFirstDeploy()) { // 有非首次安裝需要切換新舊版
						needSwitch = true;
					}
					grayscalService.add(deployData);
				} else {
					return CommonParam.DEPLOY_FAILED;
				}
			} else {
				// 執行單一部署
				if (!deployService(deployData, getDefaultDeploymentType())) {
					return CommonParam.DEPLOY_FAILED;
				}
			}
			loggerSplit("服務 " + deployData.getServiceName() + "部署結束");
		}
		// 當有執行灰度部署時，在所有服務部署完畢後做切換，並且刪除舊的 container
		if (hasGrayscale) {
			loggerSplit("服務新舊版本切換開始");
			if (needReloadNginx) {
				loggerStep("灰度部署服務切換");
				switchingService();
			} else {
				loggerRuntime("Nginx config 沒有變更，不需要切換");
			}
			if (needSwitch) {
				loggerStep("關閉舊版服務");
				shutdownService(grayscalService);
			} else {
				loggerRuntime("服務皆為首次安裝，不需要切換新舊版");
			}
			loggerSplit("服務新舊版本切換結束");
		}
		return CommonParam.DEPLOY_SUCCESS;
	}

	/**
	 * 執行單一部署
	 * 
	 * @param serviceManager
	 * @param upgradeData
	 * @throws SQLException
	 */
	private boolean deployService(DeployData deployData, String deploymentType) throws Exception {
		String serviceName = deployData.getServiceName();
		String imageName = deployData.getUpgradeImage();
		String releaseVersion = serviceManager.getVersionFromImage(imageName);
		deployLogManager.setImage(serviceName, imageName);
		// 取得 image
		if (!serviceManager.pullImage(imageName)) {
			String msg = "Image 錯誤 : " + serviceName + ":" + releaseVersion + " 找不到 image。";
			loggerRuntime(msg);
			deployLogManager.setErrorMessage(serviceName, msg);
			return false;
		}

		// 取得部署參數
		List<Map<String, Object>> details = DeployDao.getAreaServiceParameterDetail(areaID, serviceName, deploymentType);
		if (details == null || details.size() == 0) {
			String msg = "找不到部署參數";
			loggerRuntime(msg);
			deployLogManager.setErrorMessage(serviceName, msg);
			return false;
		}
		String deployName = ""; // 取得服務部署名稱
		for (Map<String, Object> detail : details) {
			if (detail.get("Parameter").equals("Name")) {
				deployName = detail.get("Source").toString();
				break;
			}
		}
		// 以部署名稱取得當前運行服務
		loggerStep("尋找是否有相同名稱的服務");
		Map<String, String> runningService = serviceManager.getServiceByName(deployName);
		if (runningService != null) {
			loggerStep("停止運行中服務");
			serviceManager.removeService(runningService.get("ID"));
		}
		loggerStep("建立服務");
		String cmd = serviceManager.createService(details, imageName);
		if (StringUtil.isEmptyOrSpace(cmd)) {
			deployLogManager.setErrorMessage(serviceName, "無法產生部署指令，請確認是否有設定部署指令規則。");
			return false;
		}
		// 紀錄部署指令
		deployLogManager.setDeployCmd(serviceName, cmd);
		// 重新取得運行中的服務
		loggerStep("重新取得運行中的服務");
		runningService = serviceManager.getServiceByName(deployName);

		// 刪除正在執行外的images
		loggerStep("刪除正在執行外的 images");
		serviceManager.rmiImages(runningService);
		// 部署紀錄
		if (runningService != null) {
			// 最新版的檔案
			if (releaseVersion.compareTo(runningService.get("version")) == 0) {
				// 最新版本==運行中版本 則更新
				deployLogManager.upgradeSuccess(serviceName);
				return true;
			} else {
				// 更新後的版本不一致
				deployLogManager.setErrorMessage(serviceName, "更新後的版本不一致");
				return false;
			}
		}
		return false;
	}

	/**
	 * 執行灰度部署
	 * 
	 * @param upgradeData
	 * @throws SQLException
	 */
	private boolean deployServiceByGrayscale(DeployData deployData) throws Exception {
		String serviceName = deployData.getServiceName();
		List<Map<String, Object>> deploymentTypes = DeployDao.getAreaServiceDeploymentTypes(areaID, serviceName);
		if (deploymentTypes == null || deploymentTypes.size() == 0) {
			String msg = "服務 " + serviceName + " 找不到部署類型設定，無法執行灰度部署。";
			loggerRuntime(msg);
			deployLogManager.setErrorMessage(serviceName, msg);
			return false;
		}
		if (deployData.isFirstDeploy()) {
			// 首次安裝，取完部署類型之後，直接部署
			if (!StringUtil.isEmptyOrSpace(deployData.getDefaultType())) {
				// 預設部署類型
				loggerRuntime("預設部署類型 : " + deployData.getDefaultType());
				deployData.setWaitDeploymentType(deployData.getDefaultType());
				return deployService(deployData, deployData.getDefaultType());
			}
			// 取第一組 Type 做部署
			if (deploymentTypes.size() > 0) {
				// 預設部署類型取不到則以資料庫第一筆類型為主
				loggerRuntime("部署類型 : " + deployData.getDefaultType());
				String deploymentType = deploymentTypes.get(0).get("DeploymentType").toString();
				deployData.setWaitDeploymentType(deploymentType);
				return deployService(deployData, deploymentType);
			}
		} else {
			// 更新
			// 取得當前部署類型
			Map<String, Object> result = DeployDao.getAreaServiceDeploymentType(areaID, serviceName, deployData.getDeployName());
			String runDeploymentType = result.get("DeploymentType").toString();
			deployData.setRunDeploymentType(runDeploymentType);
			// 紀錄等待部署類型
			for (Map<String, Object> typeMap : deploymentTypes) {
				if (!runDeploymentType.equals(typeMap.get("DeploymentType"))) {
					deployData.setWaitDeploymentType(typeMap.get("DeploymentType").toString());
				}
			}
			if (deployService(deployData, deployData.getWaitDeploymentType())) {
				// 部署後，在做 nginx 設定
				// 取得服務的 nginx 設定
				List<Map<String, Object>> results = DeployDao.getAreaNginxParameterByService(areaID, serviceName);
				if (results == null || results.size() == 0) {
					// 服務沒有設定 nginx 不需要做 config 變更
					loggerRuntime("服務 " + serviceName + " 沒有設定 Nginx，不做 config 變更。");
					return true;
				}
				// 取出所有服務對應權重設定
				Map<String, String> serviceWeight = nginxManager.searchWeight();
				// 取得當前服務設定值
				String weight = serviceWeight.get(serviceName);
				String waitReponseIP = "";
				for (Map<String, Object> res : results) {
					if (deployData.getWaitDeploymentType().equals(res.get("DeploymentType"))) {
						waitReponseIP = res.get("ReponseIP").toString();
						break;
					}
				}
				// 更新 nginx
				loggerStep("修改 Nginx config");
				loggerRuntime("服務 " + serviceName + " 修改 Nginx config");
				nginxManager.updateServiceWeight(serviceName, weight, waitReponseIP);
				deployData.setUpgradeNginx(true);
				return true;
			} else {
				return false;
			}
		}
		return false;
	}

	/**
	 * 切換灰度部署服務
	 */
	private void switchingService() {
		String time = "0";
		if (!StringUtil.isEmptyOrSpace(areaConfig.get("WaitingTime"))) {
			time = (String) areaConfig.get("WaitingTime");
		}
		loggerRuntime("等待 " + time + " 秒，等待服務啟動開始切換 Nginx");
		// 等待服務啟動完成
		sleep();
		// 覆蓋遠端服務器的 nginx config
		nginxManager.replaceConfig();
		nginxManager.reload();
		loggerRuntime("等待 " + time + " 秒，服務流量切換至新版 container");
		// 等待服務流量切換至新的 container
		sleep();
	}

	/**
	 * 關閉舊版 container
	 * 
	 * @param grayscalService
	 * @throws Exception
	 */
	private boolean shutdownService(List<DeployData> grayscalService) throws Exception {
		for (DeployData deployData : grayscalService) {
			if (deployData.isFirstDeploy()) {
				continue;
			}
			// 以部署名稱取得當前運行服務
			Map<String, String> runningService = serviceManager.getServiceByName(deployData.getDeployName());
			if (runningService != null) {
				loggerStep("停止舊版服務 : " + runningService.get("NAME"));
				serviceManager.removeService(runningService.get("ID"));
			}
		}
		return true;
	}

	/**
	 * 取得預設的部署類型
	 * 
	 * @return
	 */
	private String getDefaultDeploymentType() {
		if (serviceType.equals(CommonParam.TYPE_APPLICATION_SERVICE)) {
			return "Default";
		} else if (serviceType.equals(CommonParam.TYPE_SYSTEM_SERVICE)) {
			return "System";
		}
		return "";
	}

	/**
	 * 取出部署區服務
	 * 
	 * @return
	 * @throws SQLException
	 */
	private Map<String, Object> getAreaService() throws Exception {
		return getAreaService(areaID);
	}

	/**
	 * 取出部署區服務
	 * 
	 * @return
	 * @throws SQLException
	 */
	private Map<String, Object> getAreaService(String areaID) throws Exception {
		List<Map<String, Object>> services = null;
		if (serviceType.equals(CommonParam.TYPE_APPLICATION_SERVICE)) {
			services = DeployDao.getAreaAppService(areaID, isAuto);
		} else if (serviceType.equals(CommonParam.TYPE_SYSTEM_SERVICE)) {
			services = DeployDao.getAreaSysService(areaID);
		}
		if (services == null || services.size() == 0) {
			throw new Exception("找不到服務設定。");
		} else {
			Map<String, Object> serviceMap = new HashMap<String, Object>();
			for (Map<String, Object> service : services) {
				serviceMap.put(service.get("ServiceName").toString(), service);
			}
			return serviceMap;
		}
	}

	/**
	 * 取得部署區該類型所有服務
	 * 
	 * @param areaID
	 * @return
	 * @throws Exception
	 */
	private Map<String, Object> getAreaAllService(String areaID) throws Exception {
		List<Map<String, Object>> services = null;
		services = DeployDao.getAreaAllService(areaID, serviceType);
		if (services == null || services.size() == 0) {
			throw new Exception("找不到服務設定。");
		} else {
			Map<String, Object> serviceMap = new HashMap<String, Object>();
			for (Map<String, Object> service : services) {
				serviceMap.put(service.get("ServiceName").toString(), service);
			}
			return serviceMap;
		}
	}

	/**
	 * 取出部署區發佈檔案位置
	 * 
	 * @return
	 * @throws SQLException
	 */
	private List<String> getAreaReleasePath() throws SQLException {
		List<Map<String, Object>> results = DeployDao.getAreaReleasePath(areaID);
		if (results != null && results.size() > 0) {
			List<String> paths = new ArrayList<String>();
			for (Map<String, Object> res : results) {
				if (res.get("ReleasePath") != null) {
					paths.add(res.get("ReleasePath").toString());
				}
			}
			return paths;
		}
		return null;
	}

	/**
	 * 判斷是否需要更新
	 * 
	 * @return
	 */
	private boolean needUpdate(String releaseVersion, String runningVersion) {
		if (runningVersion == null) {
			return true; // 沒有運行中的服務直接更新
		}
		if (releaseVersion == null) {
			return false; // 沒有更新檔
		}
		if (releaseVersion.equals(runningVersion)) {
			return false; // 發布版本==運行中版本 則不更新
		}
		if (releaseVersion.compareTo(runningVersion) > 0) {
			return true; // 發布版本 > 運行中版本 則更新
		}
		return true;
	}

	/**
	 * 檢查服務相依
	 * 
	 * @param upgradeServiceList
	 * @param runningServiceList
	 * @return
	 * @throws Exception
	 */
	private boolean checkDependService(List<String> upgradeServiceList, List<Map<String, String>> runningServiceList) throws Exception {
		List<String> errorMsgs = new ArrayList<String>();
		for (String serviceName : upgradeServiceList) {
			List<Map<String, Object>> results = DeployDao.getAreaServiceDepend(areaID, serviceName);
			// 檢查服務相依是否符合條件
			if (results != null && results.size() > 0) {
				for (Map<String, Object> res : results) {
					// 檢查是否存在本次部署服務範圍
					if (!upgradeServiceList.contains(res.get("DependOn"))) {
						// 不存在則檢查該相依的服務是否正在運行中
						boolean hasRunning = false;
						if (runningServiceList != null && runningServiceList.size() > 0) {
							for (Map<String, String> runningService : runningServiceList) {
								if (runningService.get("NAME").indexOf(res.get("DependOn").toString()) != -1) {
									hasRunning = true;
								}
							}
						}
						if (!hasRunning) {
							String msg = "服務相依檢查失敗，" + serviceName + " 缺少相依服務 : " + res.get("DependOn");
							errorMsgs.add(msg);
							deployLogManager.setErrorMessage(serviceName, msg);
						}
					}
				}
			}
		}
		if (errorMsgs.size() > 0) {
			for (String msg : errorMsgs) {
				loggerRuntime(msg);
			}
			return false;
		}
		return true;
	}

	/**
	 * 灰度部署相依檢查
	 * 
	 * @param deployDataList
	 * @throws Exception
	 */
	private void checkDependOnGrayscale(List<DeployData> deployDataList, List<Map<String, String>> runningServiceList) throws Exception {
		Map<String, Object> allServices = getAreaAllService(areaID);
		List<DeployData> extraDataList = new ArrayList<DeployData>();
		for (DeployData data : deployDataList) {
			String serviceName = data.getServiceName();
			if (data.isGrayscale()) {
				// 取得向上相依服務
				List<Map<String, Object>> results = DeployDao.getAreaServiceDepend(areaID, serviceName);
				if (results != null && results.size() > 0) {
					for (Map<String, Object> res : results) {
						// 檢查是否存在本次部署服務範圍
						boolean inDeploy = false;
						for (DeployData deployData : deployDataList) {
							if (deployData.getServiceName().equals(res.get("DependOn"))) {
								inDeploy = true;
								break;
							}
						}
						// 服務不在部署範圍，因灰度部署原則，則取運行中版本做灰度
						if (!inDeploy) {
							if (runningServiceList != null && runningServiceList.size() > 0) {
								for (Map<String, String> runningService : runningServiceList) {
									if (runningService.get("NAME").indexOf(res.get("DependOn").toString()) != -1) {
										Map<String, Object> service = (Map<String, Object>) allServices.get(res.get("DependOn").toString());
										loggerRuntime("服務 " + serviceName + " 因灰度部署需求增加 " + res.get("DependOn").toString() + " 同時部署。");
										DeployData runningData = new DeployData();
										runningData.setServiceName(res.get("DependOn").toString());
										runningData.setRunImage(runningService.get("IMAGE"));
										runningData.setUpgradeImage(runningService.get("IMAGE"));
										runningData.setFirstDeploy(false);
										runningData.setSyncDeploy(true);
										runningData.setDeployName(runningService.get("NAME"));
										runningData.setGrayscale(true);
										runningData.setQueue(Integer.valueOf(service.get("Queue").toString()));
										extraDataList.add(runningData);
									}
								}
							}
						}
					}
				}
			}
		}
		if (extraDataList.size() > 0) {
			deployDataList.addAll(extraDataList);
		}
	}

	/**
	 * 服務等待時間
	 */
	private void sleep() {
		try {
			String time = "0";
			if (!StringUtil.isEmptyOrSpace(areaConfig.get("WaitingTime"))) {
				time = (String) areaConfig.get("WaitingTime");
			}
			long longTime = Long.parseLong(time);
			Thread.sleep(longTime * 1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 分隔線
	 * 
	 * @param msg
	 */
	private static void loggerSplit(String msg) {
		logger("-------------------------------------- " + msg + " -------------------------------------");
	}

	/**
	 * 步驟日誌
	 * 
	 * @param msg
	 */
	private static void loggerStep(String msg) {
		logger("");
		logger("Step >> " + msg);
		logger("");
	}

	/**
	 * 運行日誌
	 * 
	 * @param msg
	 */
	private static void loggerRuntime(String msg) {
		logger("---> " + msg);
	}

	/**
	 * 寫入 log
	 * 
	 * @param msg
	 */
	private static void logger(String msg) {
		// System.out.println(msg);
		LoggerUtil.info(msg);
	}
}
