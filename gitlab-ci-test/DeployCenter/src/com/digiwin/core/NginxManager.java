package com.digiwin.core;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.digiwin.utils.LoggerUtil;
import com.github.odiszapc.nginxparser.NgxBlock;
import com.github.odiszapc.nginxparser.NgxConfig;
import com.github.odiszapc.nginxparser.NgxDumper;
import com.github.odiszapc.nginxparser.NgxEntry;
import com.github.odiszapc.nginxparser.NgxParam;
import com.digiwin.utils.ExecCmdUtil;

public class NginxManager {
	private Map<String, Object> areaConfig = null;
	private ExecCmdUtil execCmd = null;
	private static String nginxConfServerPath = null;
	private static String nginxConfLocalPath = null;
	private static String reloadCmd = null;
	private NgxConfig ngxConfig = null;

	public NginxManager(Map<String, Object> areaConfig) {
		this.areaConfig = areaConfig;
		getServerConnection();
		init();
	}

	/****
	 * 伺服器連線
	 */
	private void getServerConnection() {
		String hostIP = areaConfig.get("HostIP").toString();
		String user = areaConfig.get("APAccount").toString();
		String pass = areaConfig.get("APPassword").toString();
		execCmd = new ExecCmdUtil(hostIP, user, pass);
	}

	/**
	 * nginx 路徑設定
	 */
	private void init() {
		nginxConfLocalPath = (String) areaConfig.get("LocalPath");
		nginxConfServerPath = (String) areaConfig.get("ServerPath");
		reloadCmd = (String) areaConfig.get("ReloadCmd");
	}

	/**
	 * nginx config 讀取
	 * 
	 * @return
	 * @throws IOException
	 */
	private NgxConfig readNginxConfig() throws IOException {
		if (ngxConfig == null) {
			InputStream inputStream = execCmd.getInputStream("cat " + nginxConfServerPath);
			ngxConfig = NgxConfig.read(inputStream);
			if (inputStream != null) {
				inputStream.close();
			}
		}
		return ngxConfig;
	}

	/***
	 * 查詢權重
	 * 
	 * @param ngxBlockUpstream
	 * @param ip
	 * @return
	 */
	public Map<String, String> searchWeight() {
		try {
			Map<String, String> ngxParamMap = new HashMap<String, String>();
			NgxConfig ngxConfig = readNginxConfig();
			NgxBlock ngxBlockHttp = ngxConfig.findBlock("http");
			List<NgxEntry> ngxBlockUpstream = ngxBlockHttp.findAll(NgxConfig.BLOCK, "upstream");
			for (NgxEntry ngxBlock : ngxBlockUpstream) {
				List<NgxEntry> upstream = ((NgxBlock) ngxBlock).findAll(NgxConfig.PARAM, "server");
				for (NgxEntry servers : upstream) {
					ngxParamMap.put(((NgxBlock) ngxBlock).getValue(), ((NgxParam) servers).getValues().get(0));
				}
			}
			return ngxParamMap;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

	/**
	 * 修改服務所對應的 nginx config
	 * 
	 * @param serviceName
	 * @param runReponseIP
	 * @param waitReponseIP
	 */
	public void updateServiceWeight(String serviceName, String runReponseIP, String waitReponseIP) {
		// 加入新的設定
		weight("add", serviceName, waitReponseIP);
		// 刪除舊的設定
		weight("delete", serviceName, runReponseIP);
	}

	/***
	 * 編輯 nginx 權重
	 * 
	 * @param type 類型
	 * @param inputStream nginx資料
	 * @param ip 10.40.40.40:2200
	 * @param weight 權重
	 * @return
	 */
	private void weight(String type, String serviceName, String weight) {
		try {
			NgxConfig ngxConfig = readNginxConfig();
			NgxBlock ngxBlockHttp = ngxConfig.findBlock("http");
			List<NgxEntry> ngxBlockUpstream = ngxBlockHttp.findAll(NgxConfig.BLOCK, "upstream");
			switch (type) {
			case "add":
				for (NgxEntry ngxBlock : ngxBlockUpstream) {
					String ngxBlockName = ((NgxBlock) ngxBlock).getValue();
					if (ngxBlockName.equals(serviceName)) {
						loggerRuntime("新增權重 " + ngxBlockName + ":" + weight);
						NgxParam ngxParam = new NgxParam();
						ngxParam.addValue("server " + weight);
						((NgxBlock) ngxBlock).addEntry(ngxParam);
					}
				}
				break;
			case "delete":
				for (NgxEntry ngxBlock : ngxBlockUpstream) {
					String ngxBlockName = ((NgxBlock) ngxBlock).getValue();
					if (ngxBlockName.equals(serviceName)) {
						loggerRuntime("移除權重 " + ngxBlockName + ":" + weight);
						List<NgxEntry> servers = ((NgxBlock) ngxBlock).findAll(NgxConfig.PARAM, "server");
						for (NgxEntry entry : servers) {
							List<String> serversList = ((NgxParam) entry).getValues();
							if (serversList.get(0).equals(weight)) {
								((NgxBlock) ngxBlock).remove(entry);
							}
						}
					}
					
				}
				break;
			default:
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 產生新的 config，並且遠端覆蓋檔案
	 */
	public void replaceConfig() {
		loggerRuntime("開始切換 Nginx ");
		String nginxConfig = new NgxDumper(ngxConfig).dump();
		buildNginxConfig(nginxConfLocalPath, nginxConfig);
		execCmd.setLocalFileToServerFolder(nginxConfLocalPath, nginxConfServerPath);
		ngxConfig = null;
	}

	/**
	 * nginx 平滑更新 config，套用新的 config 設定
	 */
	public void reload() {
		loggerRuntime("Nginx Reload");
		execCmd.backList(reloadCmd + " -s reload ");
	}

	/**
	 * 產生 nginx config 並存在本地資料夾
	 * 
	 * @param configPath
	 * @param sr
	 */
	private void buildNginxConfig(String configPath, String sr) {
		try {
			File file = new File(configPath);
			FileWriter fw = new FileWriter(file, false);
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(sr);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * 運行日誌
	 * 
	 * @param msg
	 */
	private void loggerRuntime(String msg) {
		logger("---> " + msg);
	}

	/**
	 * 寫入 log
	 * 
	 * @param msg
	 */
	private void logger(String msg) {
		// System.out.println(msg);
		LoggerUtil.info(msg);
	}
}
