package com.digiwin.core;

import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.digiwin.utils.DeployDao;
import com.digiwin.utils.DockerCmd;
import com.digiwin.utils.ExecCmdUtil;
import com.digiwin.utils.LoggerUtil;
import com.digiwin.utils.StringUtil;

public class ReleaseManager {
	/**
	 * 副檔名 .tar
	 */
	private final static String EXTENSION_TAR = ".tar";
	/**
	 * 副檔名 .tgz
	 */
	private final static String EXTENSION_GZIP = ".tar.gz";

	private Map<String, Object> areaConfig = null;
	private List<String> releasePaths = null;
	private Map<String, List<String>> releaseDirs = new HashMap<String, List<String>>();
	private String ReleasedirectoryRegex = null;
	ExecCmdUtil execCmd = null;

	public ReleaseManager(Map<String, Object> areaConfig, List<String> paths) throws Exception {
		this.areaConfig = areaConfig;
		releasePaths = paths;
		initDeployConfig();
		getServerConnection();
		getReleaseDirs();

	}

	/**
	 * 讀取部署中心參數，設定版本資料夾規則
	 * 
	 * @throws Exception
	 */
	private void initDeployConfig() throws Exception {
		Map<String, Object> config = DeployDao.getDeployConfig(areaConfig.get("AreaID").toString(), "ReleasedirectoryRegex");
		if (config != null && !StringUtil.isEmptyOrSpace(config.get("Value"))) {
			String value = config.get("Value").toString();
			ReleasedirectoryRegex = value;
		} else {
			throw new Exception("部署中心參數設定錯誤 : deployConfig ");
		}
	}

	/****
	 * 伺服器連線
	 * 
	 * @throws SQLException
	 */
	private void getServerConnection() throws SQLException {
		String versionBasisHostID = areaConfig.get("VersionBasisHostID").toString();
		List<Map<String, Object>> hostConfig = DeployDao.getHostConfig(versionBasisHostID);
		String hostIP = hostConfig.get(0).get("HostIP").toString();
		String user = hostConfig.get(0).get("APAccount").toString();
		String pass = hostConfig.get(0).get("APPassword").toString();
		execCmd = new ExecCmdUtil(hostIP, user, pass);
	}

	/**
	 * 取出發佈區目前所有版本資料夾，並按照發佈位置儲存
	 */
	private void getReleaseDirs() {
		for (String releasePath : releasePaths) {
			// 取出遠端伺服器資料夾的內容
			List<String> subNameList = execCmd.backList("ls " + releasePath, false);
			List<String> releaseVersionDir = new ArrayList<String>();
			// 過濾內容是否為發布版本資料夾
			for (String subName : subNameList) {
				// 以正規表示式過濾 格式為 1.0.0
				if (subName.matches(ReleasedirectoryRegex)) {
					releaseVersionDir.add(subName);
				}
			}
			if (releaseVersionDir.size() > 0) {
				// 資料夾排序由 新版->舊版
				Collections.sort(releaseVersionDir, new Comparator<String>() {
					@Override
					public int compare(String o1, String o2) {
						// 排序
						return o2.compareTo(o1);
					}
				});
				releaseDirs.put(releasePath, releaseVersionDir);
			}
		}
	}

	/**
	 * 取得發佈區所有完整路徑
	 * 
	 * @return
	 */
	private List<String> getCompleteReleasePaths() {
		List<String> completeReleasePaths = new ArrayList<String>();
		// 讀取發佈路徑
		for (String releasePath : releasePaths) {
			if (!releaseDirs.containsKey(releasePath)) {
				continue;
			}
			List<String> releaseVersionDirs = releaseDirs.get(releasePath);
			// 讀取版本資料夾
			for (String versionDir : releaseVersionDirs) {
				// 完整路徑
				String path = releasePath + versionDir + "/";
				completeReleasePaths.add(path);
			}
		}
		return completeReleasePaths;
	}

	/**
	 * 取得所有發佈檔案
	 * 
	 * @return
	 */
	public Map<String, Object> getReleaseFiles(Map<String, Object> serviceMap) {
		Map<String, Object> releaseFiles = new HashMap<String, Object>();
		List<String> completeReleasePaths = getCompleteReleasePaths();
		// 根據服務開始搜尋更新檔
		for (String serviceName : serviceMap.keySet()) {
			// 讀取發佈路徑
			for (String path : completeReleasePaths) {
				// 取出該資料夾檔案 list
				List<String> subNameList = execCmd.backList("ls " + path, false);
				for (String subName : subNameList) {
					Map<String, String> fileMap = new HashMap<String, String>();
					if (subName.indexOf(serviceName) == -1) {
						continue; // 不為服務發佈檔案則跳過
					}
					String filePath = path + subName;
					loggerRuntime("服務 " + serviceName + " 找到更新檔 ");
					loggerRuntime("路徑 : " + filePath);
					String version = subName.substring(subName.lastIndexOf("-") + 1, subName.indexOf(EXTENSION_TAR));
					fileMap.put("fileName", subName);
					fileMap.put("serviceName", serviceName);
					fileMap.put("version", version);
					fileMap.put("path", filePath);
					String imageName = getImageName(serviceName, version);
					fileMap.put("imageName", imageName);
					releaseFiles.put(serviceName, fileMap);
					break; // 已經找到更新擋則不再掃描其他檔案
				}
				if (releaseFiles.containsKey(serviceName)) {
					break; // 已經找到更新擋則換下一個服務
				}
			}
		}

		return releaseFiles;
	}

	private String getRegistry() {
		String registry = areaConfig.get("RegistryIP").toString();
		if (!StringUtil.isEmptyOrSpace(areaConfig.get("ClassifyPath"))) {
			registry += "/" + areaConfig.get("ClassifyPath");
		}
		return registry;
	}

	private String getImageName(String service, String version) {
		return getRegistry() + "/" + service + ":" + version;
	}

	/**
	 * 檢查檔案是否為服務發佈檔案
	 * 
	 * @param serviceMap
	 * @param fileName
	 * @return
	 */
	private boolean isServiceFile(Map<String, Object> serviceMap, String fileName) {
		for (String serviceName : serviceMap.keySet()) {
			if (fileName.indexOf(serviceName) != -1) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 將 tar 檔解開產生 image 並推送到 registry
	 * 
	 * @param fileMap
	 */
	public boolean pushToRegistry(Map<String, String> fileMap) {
		// docker load
		loggerRuntime("載入 Image ");
		String sourceName = loadImage(fileMap.get("fileName"), fileMap.get("path"));
		if (StringUtil.isEmptyOrSpace(sourceName)) {
			loggerRuntime("載入 Image 錯誤 : " + fileMap.get("fileName"));
			return false;
		}
		// docker create
		loggerRuntime("Image 重新命名");
		createImage(fileMap, sourceName);
		// docker push to registry
		loggerRuntime("Image push 到 registry");
		// loginRegistry();
		pushImage(fileMap);
		insertReleaseData(fileMap);
		return true;
	}

	/**
	 * 載入 image 更新檔
	 * 
	 * @return
	 */
	private String loadImage(String fileName, String filePath) {
		String cmd = null;
		String sourceName = "";
		String releaseName = fileName;
		String releasePath = filePath;
		if (releaseName.lastIndexOf(EXTENSION_TAR) != -1 && releaseName.lastIndexOf(EXTENSION_GZIP) == -1) {
			// tar file
			// 取得 load 指令
			cmd = DockerCmd.getLoadImageCmd(releasePath);
		} else if (releaseName.lastIndexOf(EXTENSION_GZIP) != -1) {
			// tgz file
			// 解壓縮
			String unzipCmd = DockerCmd.getUnZipFileCmd(releasePath);
			loggerCmd(unzipCmd);
			String result = execCmd.backString(unzipCmd);
			loggerResponse(result);
			// 更換副檔名
			String newFilePath = releasePath.replace(EXTENSION_GZIP, EXTENSION_TAR);
			// 取得 load 指令
			cmd = DockerCmd.getLoadImageCmd(newFilePath);
		}
		if (!StringUtil.isEmptyOrSpace(cmd)) {
			loggerCmd(cmd);
			String result = execCmd.backString(cmd);
			loggerResponse(result);

			if (!StringUtil.isEmptyOrSpace(result) && (result.indexOf("Loaded image: ") != -1 || result.indexOf("Loaded image ID: ") != -1)) {
				if (result.indexOf("Loaded image: ") != -1) {
					sourceName = result.replace("Loaded image: ", "");
				} else if (result.indexOf("Loaded image ID: ") != -1) {
					sourceName = result.replace("Loaded image ID: ", "");
				}
			}
		}
		return sourceName;
	}

	/**
	 * 重新命名 Image
	 * 
	 * @param sourceName
	 */
	private void createImage(Map<String, String> fileMap, String sourceName) {
		String rename = (String) fileMap.get("imageName");
		String cmd = DockerCmd.getCreateImageCmd(sourceName, rename);
		loggerCmd(cmd);
		String result = execCmd.backString(cmd);
		loggerResponse(result);
	}

	/**
	 * 登入 registry
	 */
	private void loginRegistry() {
		String registryIP = areaConfig.get("RegistryIP").toString();
		String registryUser = areaConfig.get("RegistryUser").toString();
		String registryPass = areaConfig.get("RegistryPass").toString();
		if (!StringUtil.isEmptyOrSpace(registryUser) && !StringUtil.isEmptyOrSpace(registryPass)) {
			String cmd = DockerCmd.getLoginRegistry(registryIP, registryUser, registryPass);
			loggerCmd(cmd);
			String result = execCmd.backString(cmd);
			loggerResponse(result);
		}
	}

	/**
	 * 推送 image 到 registry
	 */
	private void pushImage(Map<String, String> fileMap) {
		String imageName = fileMap.get("imageName");
		String cmd = DockerCmd.getPushImageCmd(imageName);
		loggerCmd(cmd);
		String result = execCmd.backString(cmd);
		loggerResponse(result);
	}

	/**
	 * 寫入發佈資料
	 * 
	 * @param host
	 * @param serviceName
	 * @param version
	 * @param updateTime
	 */
	public void insertReleaseData(Map<String, String> fileMap) {
		try {
			String code = "";
			SimpleDateFormat parser = new SimpleDateFormat("yyyyMMddHHmmss");
			code = parser.format(new Date());

			Map<String, Object> dataMap = new HashMap<String, Object>();
			dataMap.put("HostID", areaConfig.get("VersionBasisHostID"));
			dataMap.put("ServiceName", fileMap.get("serviceName"));
			dataMap.put("ServiceVersion", fileMap.get("version"));
			dataMap.put("UpdateTime", code);
			DeployDao.insertReleaseVersion(dataMap);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void loggerCmd(String cmd) {
		loggerRuntime("伺服器 : " + execCmd.getServerIP());
		loggerRuntime("執行命令 : ");
		loggerRuntime(cmd);
	}

	private void loggerResponse(String result) {
		loggerRuntime("回應結果 : ");
		loggerRuntime(result);
	}

	/**
	 * 運行日誌
	 * 
	 * @param msg
	 */
	private void loggerRuntime(String msg) {
		logger("---> " + msg);
	}

	/**
	 * 寫入 log
	 * 
	 * @param msg
	 */
	private void logger(String msg) {
		// System.out.println(msg);
		LoggerUtil.info(msg);
	}
}
