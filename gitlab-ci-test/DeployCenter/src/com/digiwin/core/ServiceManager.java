package com.digiwin.core;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.digiwin.utils.DeployDao;
import com.digiwin.utils.DockerCmd;
import com.digiwin.utils.ExecCmdUtil;
import com.digiwin.utils.LoggerUtil;
import com.digiwin.utils.StringUtil;

public class ServiceManager {
	private final static String KEY_IMAGE = "IMAGE";
	private final static String KEY_ID = "ID";
	private final static String KEY_NAME = "NAME";

	ExecCmdUtil execCmd = null;
	private Map<String, Object> areaConfig = null;

	/**
	 * registry 是否登入驗證
	 */
	private boolean registryAuth = false;

	public ServiceManager(Map<String, Object> areaConfig) {
		this.areaConfig = areaConfig;
		getServerConnection();
	}

	/****
	 * 伺服器連線
	 */
	private void getServerConnection() {
		String hostIP = areaConfig.get("HostIP").toString();
		String user = areaConfig.get("APAccount").toString();
		String pass = areaConfig.get("APPassword").toString();
		execCmd = new ExecCmdUtil(hostIP, user, pass);
	}

	/**
	 * 取出全部的 service
	 * 
	 * @return
	 */
	public List<Map<String, String>> getServiceList() {
		String cmd = DockerCmd.getServiceList();
		loggerCmd(cmd);
		List<String> readLines = execCmd.backList(cmd);
		if (readLines != null && readLines.size() >= 2) {
			List<Map<String, String>> results = dockerMsgToMap(readLines);
			return results;
		}
		return null;
	}

	/**
	 * 根據服務名稱取得 service
	 * 
	 * @param serviceName
	 * @return
	 */
	public Map<String, String> getServiceByName(String serviceName) {
		String cmd = DockerCmd.getServiceByName(serviceName);
		loggerCmd(cmd);
		List<String> readLines = execCmd.backList(cmd);
		if (readLines != null && readLines.size() >= 2) {
			List<Map<String, String>> results = dockerMsgToMap(readLines);
			Map<String, String> service = results.get(0);
			service.put("version", getVersionFromImage(service.get(KEY_IMAGE)));
			return service;
		}
		return null;
	}

	/**
	 * 取得 image 版本
	 * 
	 * @param image
	 * @return
	 */
	public String getVersionFromImage(String image) {
		String version = image.substring(image.lastIndexOf(":") + 1, image.length());
		return version;
	}

	/**
	 * 將 server 回應的 service 資訊重新組成 map 物件
	 * 
	 * @param readLine
	 * @return
	 */
	private List<Map<String, String>> dockerMsgToMap(List<String> readLines) {
		// 取 title
		String titleLine = readLines.get(0);
		String[] titleSplit = readLines.get(0).split(" ");
		List<String> columns = new ArrayList<String>();
		// 拆解每個欄位名稱
		for (int i = 0; i < titleSplit.length; i++) {
			if (!titleSplit[i].trim().isEmpty()) {
				columns.add(titleSplit[i]);
			}
		}

		List<Map<String, String>> results = new ArrayList<Map<String, String>>();
		for (int i = 1; i < readLines.size(); i++) {
			Map<String, String> resultMap = new HashMap<String, String>();
			String msgLine = readLines.get(i);
			int startIndex = 0; // 開始位置
			int endIndex = 0; // 結束位置
			for (int columnIndex = 0; columnIndex < columns.size(); columnIndex++) {
				// server 訊息排列，title 間格長度即為訊息長度，因此根據取得欄位的間隔長度可以拆解訊息
				// 結束位置取下一個欄位位置
				if (columnIndex + 1 < columns.size()) {
					endIndex = titleLine.indexOf(columns.get(columnIndex + 1));
				} else {
					endIndex = msgLine.length();
				}
				// 拆解訊息
				String msg = msgLine.substring(startIndex, endIndex).trim(); // 訊息拆解完後，去掉首尾空白
				resultMap.put(columns.get(columnIndex), msg);
				startIndex = endIndex;// 設定下一個欄位的起始位置
			}
			results.add(resultMap);
		}
		return results;
	}

	/**
	 * 移除服務 for service
	 * 
	 * @param id 服務 docker service id
	 */
	public void removeService(String id) {
		String cmd = DockerCmd.getRemoveServiceCmd(id);
		loggerCmd(cmd);
		String result = execCmd.backString(cmd);
		loggerResponse(result);
	}

	/**
	 * 建立 docker service
	 * 
	 * @throws SQLException
	 */
	public String createService(List<Map<String, Object>> detailValueList, String image) throws SQLException {
		StringBuilder buffer = new StringBuilder();
		// 取出參數規則
		List<Map<String, Object>> deployRuleMap = DeployDao.getServiceParameterRule();
		if (deployRuleMap == null || deployRuleMap.size() == 0) {
			return null;
		}
		// 開始組建部署參數
		buffer.append(DockerCmd.getCreateServiceStartCmd());
		if (detailValueList != null && detailValueList.size() > 0) {
			for (Map<String, Object> detailValueMap : detailValueList) {
				// 參數代號
				String parameter = (String) detailValueMap.get("Parameter");
				// 參數值來源
				String parameterSource = "";
				if (!StringUtil.isEmptyOrSpace(detailValueMap.get("Source"))) {
					parameterSource = (String) detailValueMap.get("Source");
				}
				// 參數值取代目的
				String parameterTarget = "";
				if (!StringUtil.isEmptyOrSpace(detailValueMap.get("Target"))) {
					parameterTarget = (String) detailValueMap.get("Target");
				}
				// 參數類型，對應規則，format 之後為實際命令格式
				for (Map serviceType : deployRuleMap) {
					if (serviceType.get("Parameter").equals(parameter)) {
						buffer.append(DockerCmd.getServiceFormatCmd(serviceType.get("Cmd"), serviceType.get("Rule"), parameterSource, parameterTarget));
					}
				}
			}
		}

		buffer.append(DockerCmd.getImageParamCmd(image));
		loggerCmd(buffer.toString());
		String result = execCmd.backString(buffer.toString());
		loggerResponse(result);
		return buffer.toString();
	}

	/**
	 * 從Registry 下載images
	 */
	public boolean pullImage(String imageName) {
		boolean statusMsg = true;
		String cmd = DockerCmd.getPullImageCmd(imageName);
		loggerCmd(cmd);
		Map<String, List<String>> msgMap = execCmd.backMap(cmd);
		List<String> result = msgMap.get("errorMsg");
		if (result != null && result.size() > 0) {
			statusMsg = false;
		}
		return statusMsg;
	}

	/***
	 * 刪除沒有正在執行的images
	 * 
	 * @param serviceName
	 */
	public void rmiImages(Map<String, String> runningInfo) {
		if (runningInfo != null) {
			String[] image = runningInfo.get(KEY_IMAGE).split("@");
			String[] repository = image[0].split(":");
			String cmd = DockerCmd.getFormatImageListCmd(repository[0], repository[1]);
			loggerCmd(cmd);
			List<String> imagesList = execCmd.backList(cmd);
			for (String imagesName : imagesList) {
				// 當跟執行版本一致則不需要刪除
				String imagesID = imagesName.split(":")[0];
				String imagesVersion = imagesName.split(":")[1];
				if (!imagesVersion.equals(runningInfo.get("version"))) {
					String removeCmd = DockerCmd.getRemoveImageCmd(imagesID);
					loggerCmd(removeCmd);
					List<String> imagesRmiMge = execCmd.backList(removeCmd);
					for (String mge : imagesRmiMge) {
						loggerRuntime("Rmi Images:" + mge);
					}
				}
			}
		}
	}

	private void loggerCmd(String cmd) {
		loggerRuntime("伺服器 : " + execCmd.getServerIP());
		loggerRuntime("執行命令 : ");
		loggerRuntime(cmd);
	}

	private void loggerResponse(String result) {
		loggerRuntime("回應結果 : ");
		loggerRuntime(result);
	}

	/**
	 * 運行日誌
	 * 
	 * @param msg
	 */
	private void loggerRuntime(String msg) {
		logger("---> " + msg);
	}

	/**
	 * 寫入 log
	 * 
	 * @param msg
	 */
	private void logger(String msg) {
		// System.out.println(msg);
		LoggerUtil.info(msg);
	}
}
