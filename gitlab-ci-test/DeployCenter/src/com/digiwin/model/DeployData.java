package com.digiwin.model;

public class DeployData {
	private String serviceName;
	private String deployName;
	private String runImage;
	private String upgradeImage;
	private boolean isGrayscale = false;
	private String runDeploymentType;
	private String waitDeploymentType;
	private boolean firstDeploy = false;
	private boolean syncDeploy = false;
	private Integer queue;
	private boolean upgradeNginx = false;
	private String defaultType;

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getDeployName() {
		return deployName;
	}

	public void setDeployName(String deployName) {
		this.deployName = deployName;
	}

	public String getRunImage() {
		return runImage;
	}

	public void setRunImage(String runImage) {
		this.runImage = runImage;
	}

	public String getUpgradeImage() {
		return upgradeImage;
	}

	public void setUpgradeImage(String upgradeImage) {
		this.upgradeImage = upgradeImage;
	}

	public boolean isGrayscale() {
		return isGrayscale;
	}

	public void setGrayscale(boolean isGrayscale) {
		this.isGrayscale = isGrayscale;
	}

	public String getRunDeploymentType() {
		return runDeploymentType;
	}

	public void setRunDeploymentType(String runDeploymentType) {
		this.runDeploymentType = runDeploymentType;
	}

	public String getWaitDeploymentType() {
		return waitDeploymentType;
	}

	public void setWaitDeploymentType(String waitDeploymentType) {
		this.waitDeploymentType = waitDeploymentType;
	}

	public boolean isFirstDeploy() {
		return firstDeploy;
	}

	public void setFirstDeploy(boolean firstDeploy) {
		this.firstDeploy = firstDeploy;
	}

	public Integer getQueue() {
		return queue;
	}

	public void setQueue(Integer queue) {
		this.queue = queue;
	}

	public boolean isUpgradeNginx() {
		return upgradeNginx;
	}

	public void setUpgradeNginx(boolean upgradeNginx) {
		this.upgradeNginx = upgradeNginx;
	}

	public boolean isSyncDeploy() {
		return syncDeploy;
	}

	public void setSyncDeploy(boolean syncDeploy) {
		this.syncDeploy = syncDeploy;
	}

	public String getDefaultType() {
		return defaultType;
	}

	public void setDefaultType(String defaultType) {
		this.defaultType = defaultType;
	}
	
}
