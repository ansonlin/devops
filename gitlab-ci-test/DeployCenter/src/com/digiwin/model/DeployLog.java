package com.digiwin.model;

public class DeployLog {
	private String areaID = "";
	private String serviceName = "";
	private String image = "";
	private String serviceVersion = "";
	private String deployCmd = "";
	private String ServiceStatus = "";
	private String serviceType = "";
	private String error = "";
	private String deployBy = "";
	private String updateTime = "";

	public String getAreaID() {
		return areaID;
	}

	public void setAreaID(String areaID) {
		this.areaID = areaID;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getServiceVersion() {
		return serviceVersion;
	}

	public void setServiceVersion(String serviceVersion) {
		this.serviceVersion = serviceVersion;
	}

	public String getDeployCmd() {
		return deployCmd;
	}

	public void setDeployCmd(String deployCmd) {
		this.deployCmd = deployCmd;
	}

	public String getServiceStatus() {
		return ServiceStatus;
	}

	public void setServiceStatus(String serviceStatus) {
		ServiceStatus = serviceStatus;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public String getDeployBy() {
		return deployBy;
	}

	public void setDeployBy(String deployBy) {
		this.deployBy = deployBy;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
}
