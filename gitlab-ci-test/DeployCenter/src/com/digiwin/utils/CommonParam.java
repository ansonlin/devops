package com.digiwin.utils;

public class CommonParam {
	public static final String TYPE_SYSTEM_SERVICE = "Sys";
	public static final String TYPE_APPLICATION_SERVICE = "App";
	public static final Integer DEPLOY_SUCCESS = 0;
	public static final Integer DEPLOY_FAILED = 1;
}
