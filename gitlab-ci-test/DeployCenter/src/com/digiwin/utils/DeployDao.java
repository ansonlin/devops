package com.digiwin.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class DeployDao {

	private static Connection connection;

	/****
	 * 從參數檔system.properties取得參數
	 */
	private static void initSystem() {
		Properties properties = new Properties();
		String dirPath = System.getProperty("user.dir");
		File propertiesFile = new File(dirPath, "DeployCenter.properties");
		try {
			InputStream configFile = propertiesFile.toURI().toURL().openStream();
			properties.load(configFile);
			conection(properties.getProperty("dbHost"), properties.getProperty("username"), properties.getProperty("password"));
		} catch (SQLException e) {
			e.printStackTrace();
			logger(e.getMessage());
			return;
		} catch (FileNotFoundException ex) {
			ex.printStackTrace();
			logger(ex.getMessage());
			return;
		} catch (IOException ex) {
			ex.printStackTrace();
			logger(ex.getMessage());
			return;
		}
	}

	/**
	 * 連接connection
	 * 
	 * @param model
	 * @throws SQLException
	 */
	public static void conection(String host, String dbUser, String dbPassword) throws SQLException {
		connection = DriverManager.getConnection(host, dbUser, dbPassword);
		logger("連線到資料庫 : " + host);
	}

	/**
	 * 關閉connection
	 * 
	 * @throws SQLException
	 */
	public void stop() throws SQLException {
		if (connection != null) {
			connection.close();
			logger("資料庫連線關閉。");
		}
	}

	/**
	 * 判斷connection是否存活，若死掉則重新連線
	 * 
	 * @throws SQLException
	 */
	public static void isConection() throws SQLException {
		if (connection == null || (connection != null && connection.isClosed())) {
			initSystem();
		}
	}

	/***
	 * 執行指令
	 * 
	 * @param sql
	 * @return
	 */
	public static boolean executeSql(String sql) {
		try {
			isConection();
			Statement stm = connection.createStatement();
			stm.executeUpdate(sql);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			logger("執行命令失敗");
		}
		return false;
	}

	public static boolean executeSqlByPrepareStatement(String sql, Map<String, Object> dataMap) {
		try {
			isConection();
			PreparedStatement pst = connection.prepareStatement(sql);
			int i = 1;
			for (String key : dataMap.keySet()) {
				pst.setString(i, dataMap.get(key).toString());
				i++;
			}
			pst.executeUpdate(sql);
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			logger("執行命令失敗");
		}
		return false;
	}

	/**
	 * 取得部署參數
	 * 
	 * @param configName
	 * @return
	 * @throws SQLException
	 */
	public static Map<String, Object> getDeployConfig(String areaID,String configName) throws SQLException {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT Value FROM deployConfig ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND AreaID='" + areaID + "' ");
		sql.append(" AND Name='" + configName + "' ");
		dataMap = getSqlMapData(sql.toString());
		return dataMap;
	}

	/**
	 * 取得部署區配置
	 * 
	 * @param areaID
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> getAreaConfig(String areaID) throws SQLException {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * FROM areaConfig ac ");
		sql.append(" LEFT JOIN hostArea ha ON ac.AreaID=ha.AreaID ");
		sql.append(" LEFT JOIN hostConfig hc ON ha.HostID = hc.HostID ");
		sql.append(" LEFT JOIN registryConfig rc ON ac.RegistryID = rc.RegistryID ");
		sql.append(" LEFT JOIN registryClassify rcf ON ac.ClassifyID = rcf.ClassifyID AND rcf.RegistryID = rc.RegistryID ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND ac.AreaID='" + areaID + "' ");
		dataList = getSqlListData(sql.toString());
		return dataList;
	}

	/**
	 * 取得伺服器配置
	 * 
	 * @param hostID
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> getHostConfig(String hostID) throws SQLException {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * FROM hostConfig ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND HostID='" + hostID + "' ");
		dataList = getSqlListData(sql.toString());
		return dataList;
	}

	/**
	 * 根據主機編號取得主機對應區域
	 * 
	 * @param hostID
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> getHostArea(String hostID) throws SQLException {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * FROM hostArea ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND HostID='" + hostID + "' ");
		dataList = getSqlListData(sql.toString());
		return dataList;
	}

	/**
	 * 取得部署區管理服務，並且允許手動部署
	 * 
	 * @param areaID 部署區編號
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> getAreaSysService(String areaID) throws SQLException {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * FROM areaService ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND AreaID='" + areaID + "' ");
		sql.append(" AND ServiceType='Sys' ");
		sql.append(" AND ManualDeploy='1' ");
		sql.append(" ORDER BY Queue ASC ");
		dataList = getSqlListData(sql.toString());
		return dataList;
	}

	/**
	 * 取得部署區應用服務
	 * 
	 * @param areaID 部署區編號
	 * @param isAuto 是否自動部署
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> getAreaAppService(String areaID, boolean isAuto) throws SQLException {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT aser.* ");
		// sql.append(" (SELECT COUNT(*) FROM areaNginxParameter anp WHERE anp.AreaID = aser.AreaID AND anp.ServiceName = aser.ServiceName ) as isGrayscale ");
		sql.append(" FROM areaService aser ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND aser.AreaID='" + areaID + "' ");
		sql.append(" AND aser.ServiceType='App' ");
		if (isAuto) {
			sql.append(" AND aser.AutoDeploy='1' ");
		} else {
			sql.append(" AND aser.ManualDeploy='1' ");
		}
		sql.append(" ORDER BY aser.Queue ASC ");
		dataList = getSqlListData(sql.toString());
		return dataList;
	}

	/**
	 * 取得部署區該類型所有服務
	 * 
	 * @param areaID
	 * @param serviceType
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> getAreaAllService(String areaID, String serviceType) throws SQLException {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT aser.* ");
		sql.append(" FROM areaService aser ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND aser.AreaID='" + areaID + "' ");
		sql.append(" AND aser.ServiceType='" + serviceType + "' ");
		sql.append(" ORDER BY aser.Queue ASC ");
		dataList = getSqlListData(sql.toString());
		return dataList;
	}

	/**
	 * 取得部署區發佈檔案位置
	 * 
	 * @param areaID
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> getAreaReleasePath(String areaID) throws SQLException {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * FROM areaReleasePath ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND AreaID='" + areaID + "' ");
		dataList = getSqlListData(sql.toString());
		return dataList;
	}

	/**
	 * 取得服務相依設定
	 * 
	 * @param areaID
	 * @param serviceName
	 * @return
	 */
	public static List<Map<String, Object>> getAreaServiceDepend(String areaID, String serviceName) {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * FROM areaServiceDepend ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND AreaID='" + areaID + "' ");
		sql.append(" AND ServiceName='" + serviceName + "' ");
		dataList = getSqlListData(sql.toString());
		return dataList;
	}

	/*
	 * 
	 * @param model
	 * 
	 * @throws SQLException
	 */
	public static Boolean insertReleaseVersion(Map<String, Object> dataMap) throws SQLException {
		String sql = getMapToInsertSql("versionFromRelease", dataMap);
		return executeSql(sql);
	}

	/***
	 * 取得參數類型
	 * 
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> getServiceParameterRule() throws SQLException {
		List<Map<String, Object>> dataTypeList = new ArrayList<Map<String, Object>>();
		String sql = "SELECT * FROM serviceParameterRule ";
		dataTypeList = getSqlListData(sql);
		return dataTypeList;
	}

	/**
	 * 取得服務部署參數
	 * 
	 * @param areaID
	 * @param serviceName
	 * @param deploymentType
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> getAreaServiceParameterDetail(String areaID, String serviceName, String deploymentType) throws SQLException {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * FROM areaServiceParameterDetail ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND AreaID='" + areaID + "' ");
		sql.append(" AND ServiceName='" + serviceName + "' ");
		sql.append(" AND DeploymentType='" + deploymentType + "' ");
		sql.append(" ORDER BY DetailID ASC ");
		dataList = getSqlListData(sql.toString());
		return dataList;
	}

	/**
	 * 取得服務現有的部署類型
	 * 
	 * @param areaID
	 * @param serviceName
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> getAreaServiceDeploymentTypes(String areaID, String serviceName) throws SQLException {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT DeploymentType ");
		sql.append(" FROM areaServiceParameterDetail ");
		sql.append(" WHERE AreaID='" + areaID + "' ");
		sql.append(" AND ServiceName='" + serviceName + "' ");
		sql.append(" GROUP BY DeploymentType ");
		dataList = getSqlListData(sql.toString());
		return dataList;
	}

	/**
	 * 取得當前運行的服務部署類型
	 * 
	 * @param areaID
	 * @param serviceName
	 * @param deployName
	 * @return
	 * @throws SQLException
	 */
	public static Map<String, Object> getAreaServiceDeploymentType(String areaID, String serviceName, String deployName) throws SQLException {
		Map<String, Object> dataMap = new HashMap<String, Object>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT DeploymentType ");
		sql.append(" FROM areaServiceParameterDetail ");
		sql.append(" WHERE AreaID='" + areaID + "' ");
		sql.append(" AND ServiceName='" + serviceName + "' ");
		sql.append(" AND Source='" + deployName + "' ");
		dataMap = getSqlMapData(sql.toString());
		return dataMap;
	}

	/**
	 * 取得與當前設定不同的另一組 nginx 設定
	 * 
	 * @param areaID
	 * @param serviceName
	 * @param responseIP
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> getAreaNginxParameterByService(String areaID, String serviceName) throws SQLException {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		StringBuilder sql = new StringBuilder();
		sql.append(" SELECT * FROM areaNginxParameter ");
		sql.append(" WHERE 1=1 ");
		sql.append(" AND AreaID='" + areaID + "' ");
		sql.append(" AND ServiceName='" + serviceName + "' ");
		dataList = getSqlListData(sql.toString());
		return dataList;
	}

	/**
	 * 更新服務部署 Image
	 * 
	 * @param areaID
	 * @param serviceName
	 * @param image
	 */
	public static void updateAreaServiceImage(String areaID, String serviceName, String image) {
		StringBuilder sql = new StringBuilder();
		sql.append(" UPDATE areaService SET ");
		sql.append(" `Image`='" + image + "'  ");
		sql.append(" WHERE `AreaID`='" + areaID + "' ");
		sql.append(" AND `ServiceName`='" + serviceName + "' ");
		executeSql(sql.toString());
	}

	// ------------------------------------------------------//

	/****
	 * 查詢SQL取得多筆資料
	 * 
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public static List<Map<String, Object>> getSqlListData(String sql) {
		// System.out.println(sql);
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			isConection();
			Statement stm = connection.createStatement();
			ResultSet rs = stm.executeQuery(sql);
			ResultSetMetaData md = rs.getMetaData();
			int columnCount = md.getColumnCount();
			while (rs.next()) {
				Map<String, Object> rowData = new HashMap<String, Object>();
				for (int i = 1; i <= columnCount; i++) {
					rowData.put(md.getColumnName(i), rs.getObject(i));
				}
				dataList.add(rowData);
			}
			rs.close();
			stm.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dataList;
	}

	/****
	 * 查詢SQL取得第一筆資料
	 * 
	 * @param sql
	 * @return
	 * @throws SQLException
	 */
	public static Map<String, Object> getSqlMapData(String sql) {
		List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>();
		try {
			isConection();
			Statement stm = connection.createStatement();
			ResultSet rs = stm.executeQuery(sql);
			ResultSetMetaData md = rs.getMetaData();
			int columnCount = md.getColumnCount();
			while (rs.next()) {
				Map<String, Object> rowData = new HashMap<String, Object>();
				for (int i = 1; i <= columnCount; i++) {
					rowData.put(md.getColumnName(i), rs.getObject(i));
				}
				dataList.add(rowData);
			}
			rs.close();
			stm.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if (dataList != null && dataList.size() > 0) {
			return dataList.get(0);
		}
		return new HashMap<String, Object>();
	}

	/***
	 * 將Map轉成新增語法
	 * 
	 * @param tableName
	 * @param dataMap
	 * @return
	 */
	public static String getMapToInsertSql(String tableName, Map<String, Object> dataMap) {
		StringBuffer tableField = new StringBuffer();
		StringBuffer tableValue = new StringBuffer();
		for (String key : dataMap.keySet()) {
			tableField.append(key + ",");
			tableValue.append(" '" + dataMap.get(key).toString().replace("'", "''") + "',");
		}
		String sql = "INSERT INTO " + tableName + " (" + tableField.toString().substring(0, tableField.toString().length() - 1) + ") VALUES (" + tableValue.toString().substring(0, tableValue.toString().length() - 1) + ")";
		return sql;
	}

	public static String getMapToInsertSqlByPrepareStatement(String tableName, Map<String, Object> dataMap) {
		StringBuffer tableField = new StringBuffer();
		StringBuffer tableValue = new StringBuffer();
		for (String key : dataMap.keySet()) {
			tableField.append(key + ",");
			tableValue.append(" ?,");
		}
		String sql = "INSERT INTO " + tableName + " (" + tableField.toString().substring(0, tableField.toString().length() - 1) + ") VALUES (" + tableValue.toString().substring(0, tableValue.toString().length() - 1) + ")";
		return sql;
	}

	/***
	 * 將Map轉成修改語法
	 * 
	 * @param tableName
	 * @param dataMap
	 * @return
	 */
	public static String getMapToUpdateSql(String tableName, Map<String, Object> dataMap, String where) {
		StringBuffer tableValue = new StringBuffer();
		for (String key : dataMap.keySet()) {
			tableValue.append(" " + key + " = '" + dataMap.get(key) + "',");
		}
		String sql = "UPDATE " + tableName + " SET " + tableValue.toString().substring(0, tableValue.toString().length() - 1) + " WHERE " + where;
		return sql;
	}

	/**
	 * 寫入 log
	 * 
	 * @param msg
	 */
	private static void logger(String msg) {
		// System.out.println(msg);
		LoggerUtil.info(msg);
	}
}
