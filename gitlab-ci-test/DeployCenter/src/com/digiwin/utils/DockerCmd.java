package com.digiwin.utils;

import com.digiwin.utils.StringUtil;

public class DockerCmd {
	private static final String NULL_CMD = "";

	public static String getServiceList() {
		return "docker service ls";
	}

	public static String getServiceByName(String service) {
		return "docker service ls -f name=" + service;
	}

	/**
	 * 載入 image
	 * 
	 * @param filePath 更新檔位置
	 * @return linux docker cmd
	 */
	public static String getLoadImageCmd(String filePath) {
		return "docker load -i " + filePath;
	}

	/**
	 * 解壓縮 tgz 更新檔
	 * 
	 * @param filePath 更新檔位置
	 * @return linux cmd
	 */
	public static String getUnZipFileCmd(String filePath) {
		return "gunzip -d " + filePath;
	}

	/**
	 * 重新命名 image
	 * 
	 * @param sourceName load 之後取得的 id or tag
	 * @param rename 新 tag
	 * @return linux docker cmd
	 */
	public static String getCreateImageCmd(String sourceName, String rename) {
		return "docker tag " + sourceName + "  " + rename;
	}

	/**
	 * 登入 registry
	 * 
	 * @param registry registry ip
	 * @param user 帳號
	 * @param pass 密碼
	 * @return linux docker cmd
	 */
	public static String getLoginRegistry(String registry, String user, String pass) {
		return "docker login " + registry + " -u " + user + " -p " + pass;
	}

	/**
	 * 推送 image 到 registry
	 * 
	 * @param imageName image 名稱
	 * @return linux docker cmd
	 */
	public static String getPushImageCmd(String imageName) {
		return "docker push  " + imageName;
	}

	/**
	 * 刪除 service
	 * 
	 * @param id docker service id
	 * @return linux docker cmd
	 */
	public static String getRemoveServiceCmd(String id) {
		return "docker service rm " + id;
	}

	/**
	 * 建立 service 開頭
	 * 
	 * @return linux docker cmd
	 */
	public static String getCreateServiceStartCmd() {
		return "docker  service create ";
	}

	/**
	 * 根據規則設定產生相應命令
	 * 
	 * @param cmd
	 * @param rule
	 * @param source
	 * @param target
	 * @return
	 */
	public static String getServiceFormatCmd(Object cmd, Object rule, String source, String target) {
		if (StringUtil.isEmptyOrSpace(cmd) && StringUtil.isEmptyOrSpace(rule)) {
			return NULL_CMD;// 參數皆為空值
		} else if (!StringUtil.isEmptyOrSpace(cmd) && StringUtil.isEmptyOrSpace(rule)) {
			return " " + cmd;// 只有指令，沒有參數規則
		}
		String format = rule.toString().replace("$source", source).replace("$target", target);
		return " " + cmd + " " + format;
	}

	/**
	 * 部署 image 設定
	 * 
	 * @param imageName image 名稱
	 * @return linux docker cmd
	 */
	public static String getImageParamCmd(String imageName) {
		if (StringUtil.isEmptyOrSpace(imageName)) {
			return NULL_CMD;
		}
		return " " + imageName;
	}

	/**
	 * 從registry取得images
	 * 
	 * @param imageName image 名稱
	 * @return linux docker cmd
	 */
	public static String getPullImageCmd(String imageName) {
		if (StringUtil.isEmptyOrSpace(imageName)) {
			return NULL_CMD;
		}
		return "docker pull  " + imageName;
	}

	public static String getFormatImageListCmd(String key1, String key2) {
		return "docker images " + key1 + ":" + key2 + " --format '{{.ID}}:{{.Tag}}'";
	}
	
	public static String getRemoveImageCmd(String id){
		return "docker rmi " + id;
	}
}
