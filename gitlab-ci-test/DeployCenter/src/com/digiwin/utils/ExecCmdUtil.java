package com.digiwin.utils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class ExecCmdUtil {

	private Session sshSession = null;
	private String serverIP = null;

	public ExecCmdUtil(String linuxServer, String linuxAcc, String linuxPwd) {
		getSSHSession(linuxServer, linuxAcc, linuxPwd);
	}

	public String getServerIP() {
		return serverIP;
	}

	/**
	 * 取得 ssh 連線 port 22
	 * 
	 * @param linuxServer linux ip
	 * @param linuxAcc linux 帳號
	 * @param linuxPwd linux 密碼
	 * @return 成功與否
	 * @throws JSchException
	 */
	public boolean getSSHSession(String linuxServer, String linuxAcc, String linuxPwd) {
		if (sshSession != null) {
			sshSession.disconnect();
		}
		Properties config = new Properties();
		config.put("StrictHostKeyChecking", "no");
		JSch jsch = new JSch();
		try {
			serverIP = linuxServer;
			sshSession = jsch.getSession(linuxAcc, linuxServer, 22);
			sshSession.setPassword(linuxPwd);
			sshSession.setConfig(config);
			sshSession.connect();
			return true;
		} catch (JSchException e) {
			System.out.println("error to do get Session ...");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}

	/***
	 * 關閉連線
	 */
	public void setSSHSession() {
		if (sshSession != null) {
			sshSession.disconnect();
		}
	}

	/**
	 * 執行 linux cmd
	 * 
	 * @param cmd
	 * @return server 回應字串
	 */
	public String backString(String cmd) {
		StringBuilder buffer = new StringBuilder();
		String readline, errorLine;
		ChannelExec channel = null;
		try {
			channel = (ChannelExec) sshSession.openChannel("exec");
			channel.setCommand(cmd);
			channel.connect();
			InputStream in = channel.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			BufferedReader errorbr = new BufferedReader(new InputStreamReader(channel.getErrStream()));
			loggerRuntime("伺服器回應 : ");
			while (true) {
				while ((readline = br.readLine()) != null) {
					loggerRuntime(readline);
					buffer.append(readline);
				}
				if (channel.isClosed()) {
					if (in.available() > 0) {
						continue;
					}
					break;
				}
			}
			while ((errorLine = errorbr.readLine()) != null) {
				loggerRuntime(errorLine);
			}
			loggerRuntime("exit: " + channel.getExitStatus());
			return buffer.toString();
		} catch (Exception e) {
			e.getMessage();
			loggerRuntime(e.getMessage());
		}
		if (channel != null) {
			channel.disconnect();
		}
		return null;
	}

	/**
	 * 執行 linux cmd Map KEY:msg與errorMsg
	 * 
	 * @param cmd
	 * @return server 回應字串
	 */
	public Map<String, List<String>> backMap(String cmd) {
		Map<String, List<String>> result = new HashMap<>();
		List<String> msgList = new ArrayList<String>();
		List<String> errorMsgList = new ArrayList<String>();
		String readline, errorLine;
		ChannelExec channel = null;
		try {
			channel = (ChannelExec) sshSession.openChannel("exec");
			channel.setCommand(cmd);
			channel.connect();
			InputStream in = channel.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			BufferedReader errorbr = new BufferedReader(new InputStreamReader(channel.getErrStream()));
			loggerRuntime("伺服器回應 : ");
			while (true) {
				while ((readline = br.readLine()) != null) {
					loggerRuntime(readline);
					msgList.add(readline);
				}
				result.put("msg", msgList);
				if (channel.isClosed()) {
					if (in.available() > 0) {
						continue;
					}
					break;
				}
			}
			while ((errorLine = errorbr.readLine()) != null) {
				loggerRuntime(errorLine);
				errorMsgList.add(errorLine);
			}
			result.put("errorMsg", errorMsgList);
			loggerRuntime("exit: " + channel.getExitStatus());
			return result;
		} catch (Exception e) {
			e.getMessage();
			loggerRuntime(e.getMessage());
		}
		if (channel != null) {
			channel.disconnect();
		}
		return null;
	}

	/**
	 * 執行 linux cmd
	 * 
	 * @param cmd
	 * @return server 回應字串
	 */
	public List<String> backList(String cmd) {
		List<String> result = new ArrayList<String>();
		String readline;
		ChannelExec channel = null;
		try {
			channel = (ChannelExec) sshSession.openChannel("exec");
			channel.setCommand(cmd);
			InputStream in = channel.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			channel.connect();
			while (true) {
				while ((readline = br.readLine()) != null) {
					result.add(readline);
					loggerRuntime(readline);
				}
				if (channel.isClosed()) {
					if (in.available() > 0) {
						continue;
					}
					break;
				}
			}
		} catch (Exception e) {
			e.getMessage();
			loggerRuntime("Server連線失敗");
		}
		if (channel != null) {
			channel.disconnect();
		}
		return result;
	}

	/**
	 * 執行 linux cmd
	 * 
	 * @param cmd
	 * @return server 回應字串
	 */
	public List<String> backList(String cmd, boolean showReturnLog) {
		List<String> result = new ArrayList<String>();
		String readline;
		ChannelExec channel = null;
		try {
			channel = (ChannelExec) sshSession.openChannel("exec");
			channel.setCommand(cmd);
			InputStream in = channel.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			channel.connect();
			while (true) {
				while ((readline = br.readLine()) != null) {
					result.add(readline);
					if (showReturnLog) {
						loggerRuntime(readline);
					}
				}
				if (channel.isClosed()) {
					if (in.available() > 0) {
						continue;
					}
					break;
				}
			}
		} catch (Exception e) {
			e.getMessage();
			loggerRuntime("Server連線失敗");
		}
		if (channel != null) {
			channel.disconnect();
		}
		return result;
	}

	/**
	 * 執行 linux cmd
	 * 
	 * @param cmd
	 * @return server 回應字串
	 */
	public InputStream getInputStream(String cmd) {
		ChannelExec channel = null;
		InputStream in = null;
		try {
			channel = (ChannelExec) sshSession.openChannel("exec");
			channel.setCommand(cmd);
			channel.connect();
			in = channel.getInputStream();
		} catch (Exception e) {
			e.getMessage();
		}
		return in;
	}

	/**
	 * 執行檔案上傳
	 * 
	 * @param localFile 本地資料夾
	 * @param serverFolder 上傳的目的地路徑
	 * @return
	 */
	public boolean setLocalFileToServerFolder(String localFile, String serverFolder) {
		try {
			Channel channel = sshSession.openChannel("sftp");
			channel.connect();
			ChannelSftp c = (ChannelSftp) channel;
			c.put(localFile, serverFolder, ChannelSftp.OVERWRITE);
		} catch (Exception e) {
			e.getMessage();
		}
		return false;
	}

	/**
	 * 運行日誌
	 * 
	 * @param msg
	 */
	private void loggerRuntime(String msg) {
		logger("---> " + msg);
	}

	/**
	 * 寫入 log
	 * 
	 * @param msg
	 */
	private void logger(String msg) {
		// System.out.println(msg);
		LoggerUtil.info(msg);
	}
}
