package com.digiwin.utils;

import org.apache.log4j.Logger;

public class LoggerUtil {
	private final static String defaultLogName = "runtime.log";

	public static Logger getLogger(String logName) {
		if (StringUtil.isEmptyOrSpace(logName)) {
			System.setProperty("deploy.log", defaultLogName);
		} else {
			System.setProperty("deploy.log", logName);
		}
		return Logger.getLogger(LoggerUtil.class);
	}

	public static Logger getLogger() {
		System.setProperty("deploy.log", defaultLogName);
		return Logger.getLogger(LoggerUtil.class);
	}

	public static void info(String msg) {
		Logger logger = getLogger();
		logger.info(msg);
	}
}
