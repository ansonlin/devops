package com.digiwin.utils;

import java.math.BigDecimal;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
/**
 * 有關字串的處理Class
 * 
 * @author Roger
 * @since 2010-5-14
 */
public class StringUtil {

	/**
	 * 取得指定字元在目標字串的位置
	 * 
	 * @param pSource
	 *            目標字串
	 * @param pSearchText
	 *            指定字串
	 * @return 指定字元在目標字串的位置
	 */
	public static int indexOfIgnoreCase(String pSource, String pSearchText) {
		return indexOfIgnoreCase(pSource, pSearchText, 0);
	}

	/**
	 * 取得指定字元在目標字串的位置
	 * 
	 * @param pSource
	 *            目標字串
	 * @param pSearchText
	 *            指定字串
	 * @param pFromIndex
	 *            搜尋起點
	 * @return 指定字元在目標字串的位置
	 */
	public static int indexOfIgnoreCase(String pSource, String pSearchText, int pFromIndex) {
		return pSource.toLowerCase().indexOf(pSearchText.toLowerCase(), pFromIndex);
	}

	/**
	 * 判斷某個字元是否在一字串中
	 * 
	 * @param pSource
	 *            被找尋的字串
	 * @param pChar
	 *            欲尋找的字元
	 * @return 尋找的結果
	 */
	public static boolean charBelongToStr(String pSource, String pChar) {
		if (isEmpty(pSource)) {
			return true;
		}
		return pSource.indexOf(pChar) > -1;
	}

	/**
	 * 依指定次數產生重覆字元
	 * 
	 * @param pSource
	 *            欲重覆的字元
	 * @param pCount
	 *            欲重覆的次數
	 * @return 產生出來的String
	 */
	public static String replicate(String pSource, int pCount) {
		StringBuffer tResult = new StringBuffer();
		for (int i = 0; i < pCount; i++) {
			tResult.append(pSource);
		}
		return tResult.toString();
	}

	/**
	 * 產生一String 以傳入的char複製pCount次為內容
	 * 
	 * @param pChar
	 *            複製字元
	 * @param pCount
	 *            複製次數
	 * @return 複製出來的結果
	 */
	public static String replicate(char pChar, int pCount) {
		StringBuffer tResult = new StringBuffer();
		for (int i = 0; i < pCount; i++) {
			tResult.append(pChar);
		}
		return tResult.toString();
	}

	/**
	 * 判斷字串是否是數值型態
	 * 
	 * @param pSource
	 *            欲判斷的字串資料
	 * @return 判斷出來的結果
	 */
	public static boolean isDigital(String pSource) {
		boolean tIsDigital = false;
		try {
			new BigDecimal(pSource);
			tIsDigital = true;
		} catch (NumberFormatException e) {
			tIsDigital = false;
		}
		return tIsDigital;
	}

	/**
	 * 判斷字串是否包含小數點，在之前會先判斷字串是否為數值型態
	 * 
	 * @param pSource
	 *            欲判斷的字串資料
	 * @return 判斷的結果
	 */
	public static boolean isInteger(String pSource) {
		if (isDigital(pSource)) {
			if (pSource.indexOf('.') < 0) {
				return true;
			}
		}
		return false;
	}

	/**
	 * 判斷是否為空值(包含null)
	 * 
	 * @param pSource
	 *            欲判斷字串
	 * @return 判斷結果
	 */
	public static boolean isEmpty(Object pSource) {
		return (pSource == null || pSource.toString().equals(""));
	}

	/**
	 * 判斷是否為空值(包含null)
	 * 
	 * @param pSource
	 *            欲判斷字串
	 * @return 判斷結果
	 */
	public static boolean isStrEmpty(Object pSource) {
		return (pSource == null || pSource.toString().equals("") || pSource.toString().equalsIgnoreCase("NULL"));
	}

	/**
	 * 判斷是否為空值(包含null)或空白
	 * 
	 * @param pObject
	 *            欲判斷Object
	 * @return 判斷結果
	 */
	public static boolean isEmptyOrSpace(Object pObject) {
		return isEmpty(pObject) || pObject.toString().trim().equals("");
	}

	/**
	 * 比較 子字串是否包含在原始字串中
	 * 
	 * @param pSourceStr
	 *            原始字串
	 * @param pSubStr
	 *            子字串
	 * @param pIgnoreCase
	 *            是否忽略大小寫
	 * @return 比較出來的結果
	 */
	public static boolean isContain(String pSourceStr, String pSubStr, boolean pIgnoreCase) {
		if (pIgnoreCase) {
			pSourceStr = pSourceStr.toUpperCase();
			pSubStr = pSubStr.toUpperCase();
		}
		Hashtable<String, Boolean> tHtSource = new Hashtable<String, Boolean>();
		// 先將SourceStr 放入 hashtable(每個字元)
		// Boolean tTempData = new Boolean("true");
		for (int i = 0; i < pSourceStr.length(); i++) {
			tHtSource.put(pSourceStr.substring(i, i + 1), Boolean.TRUE);
		}
		// 若在 原始字串的 hashtable 中找不到,則表示不包含在原始字串中
		for (int j = 0; j < pSubStr.length(); j++) {
			Object tValue = tHtSource.get(pSubStr.substring(j, j + 1));
			if (tValue == null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 合併不定個數串，不會將 null或空白 的合併進去
	 * 
	 * @param 原始字串
	 * @return 合併後的結果出來的結果
	 */
	public static String merge(String... pSourceString) {
		StringBuffer tResult = new StringBuffer();
		for (String tMergeString : pSourceString) {
			if (!isEmptyOrSpace(tMergeString)) {
				tResult.append(tMergeString);
			}
		}
		return tResult.toString();

	}

	/**
	 * 將Null字串轉換為空字串
	 * 
	 * @param pNullStr
	 * @return
	 */
	public static String convertNullToEmptyString(String pNullStr) {
		if (pNullStr == null) {
			return "";
		} else {
			return pNullStr;
		}
	}

	/**
	 * Add by Allen 2011/04/07
	 * 
	 * Encode a string using algorithm specified in web.xml and return the
	 * resulting encrypted password. If exception, the plain credentials string
	 * is returned
	 * 
	 * @param pPassword
	 *            Password or other credentials to use in authenticating this
	 *            username
	 * @param pAlgorithm
	 *            Algorithm used to do the digest
	 * 
	 * @return encypted password based on the algorithm.
	 */
	public static String encodePassword(String pPassword, String pAlgorithm) {
		byte[] tUnencodedPassword = pPassword.getBytes();

		MessageDigest tMd = null;

		try {
			// first create an instance, given the provider
			tMd = MessageDigest.getInstance(pAlgorithm);
		} catch (Exception e) {
			return pPassword;
		}

		tMd.reset();

		// call the update method one or more times
		// (useful when you don't know the size of your data, eg. stream)
		tMd.update(tUnencodedPassword);

		// now calculate the hash
		byte[] tEncodedPassword = tMd.digest();

		StringBuffer tBuffer = new StringBuffer();

		for (int i = 0; i < tEncodedPassword.length; i++) {
			if ((tEncodedPassword[i] & 0xff) < 0x10) {
				tBuffer.append("0");
			}
			tBuffer.append(Long.toString(tEncodedPassword[i] & 0xff, 16));
		}

		return tBuffer.toString();
	}

	public static String valueOf(Object o) {
		if (o == null) {
			return null;
		}
		return String.valueOf(o);
	}

	public static List<String> getStringList(String content, String beforeStrContain, String afterStrContain, String beforeStr, String afterStr) {
		List<String> list = new ArrayList<String>();
		while (content.contains(beforeStrContain)) {
			int beforeIndex = content.indexOf(beforeStrContain);
			int afterIndex = content.indexOf(afterStrContain);
			String subStrin = content.substring(beforeIndex, afterIndex + afterStrContain.length());
			content = content.replaceFirst(beforeStr, "");
			content = content.replaceFirst(afterStr, "");
			list.add(subStrin.trim());
		}
		return list;
	}

	public static List<String> getStringListOrigin(String content, String beforeStrContain, String afterStrContain, String beforeStr, String afterStr) {
		List<String> list = new ArrayList<String>();
		while (content.contains(beforeStrContain)) {
			int beforeIndex = content.indexOf(beforeStrContain);
			int afterIndex = content.indexOf(afterStrContain);
			String subString = content.substring(beforeIndex, afterIndex + afterStrContain.length());
			content = content.replaceFirst(beforeStr, "");
			content = content.replaceFirst(afterStr, "");

			subString = subString.replaceFirst(beforeStr, "");
			subString = subString.replaceFirst(afterStr, "");

			list.add(subString.trim());
		}
		return list;
	}

	/**
	 * 對$字進行轉譯 String replaceAll(regex, replacement)函數 ,
	 * 由於第一個參數支持正則表達式，replacement中出現「$」,會按照$1$2的分組 模式進行匹配，當編譯器發現「$」後不是整數的時候，
	 * 就會拋出「Illegal group reference」的異常。
	 * 
	 * @author wizards
	 * @param str
	 *            要過濾的字串
	 * @return
	 */
	public static String filterDollarStr(String str) {
		String sReturn = "";
		// if (!StringUtils.trim(str).equals("")) {
		if (!isEmptyOrSpace(str)) {
			if (str.indexOf('$', 0) > -1) {
				while (str.length() > 0) {
					if (str.indexOf('$', 0) > -1) {
						sReturn += str.subSequence(0, str.indexOf('$', 0));
						sReturn += "\\$";
						str = str.substring(str.indexOf('$', 0) + 1, str.length());
					} else {
						sReturn += str;
						str = "";
					}
				}
			} else {
				sReturn = str;
			}
		}
		return sReturn;
	}

	/**
	 * 將true | flase 字串轉換成boolean值
	 * 
	 * @param str
	 *            要轉換的字串
	 * @return 轉換後的boolean值
	 */
	public static boolean transString2Bool(String str) {
		if ("true".equalsIgnoreCase(str)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 將字串陣列轉換成HashSet
	 * @param array 字串陣列
	 * @return 轉換後的HashSet
	 */
	public static Set stringArray2HashSet(String[] array) {
		Set set = new HashSet<String>();
		for (String str : array) {
			set.add(str);
		}
		return set;
	}
	
	
	
	
	
}
