#!/bin/bash

export FGLPROFILE_RUN=${FGLDIR}/etc/fglprofile.run
export FGLPROFILE_COMP=${FGLDIR}/etc/fglprofile.comp
export FGLPROFILE=${FGLPROFILE:=$FGLPROFILE_RUN}

exec dockerize -template $FGLPROFILE_RUN.tmpl:$FGLPROFILE_RUN \
    -template $FGLPROFILE_COMP.tmpl:$FGLPROFILE_COMP \
    -template $FGLPROFILE_TMPL:${FGLDIR}/etc/main "$@"
