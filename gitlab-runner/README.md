
1. Deploy YAML file

2. Register Gitlab-runner 
reference:  https://docs.gitlab.com/runner/register/index.html

![image](https://github.com/ansonlin/devops/blob/master/gitlab-runner/gitlab-runner-register.jpg)
    
3.  Edit config.toml    
![image](https://github.com/ansonlin/devops/blob/master/gitlab-runner/config.toml.jpg)
