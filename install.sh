#!/bin/bash

#docker version: 1.11.2 
#docker-compose version: 1.7.1 


set +e
set -o noglob

item=1

#
# Set Colors
#

bold=$(tput bold)
underline=$(tput sgr 0 1)
reset=$(tput sgr0)

red=$(tput setaf 1)
green=$(tput setaf 76)
white=$(tput setaf 7)
tan=$(tput setaf 202)
blue=$(tput setaf 25)


#
# Headers and Logging
#

underline() { printf "${underline}${bold}%s${reset}\n" "$@"
}
h1() { printf "\n${underline}${bold}${blue}%s${reset}\n" "$@"
}
h2() { printf "\n${underline}${bold}${white}%s${reset}\n" "$@"
}
debug() { printf "${white}%s${reset}\n" "$@"
}
info() { printf "${white}➜ %s${reset}\n" "$@"
}
success() { printf "${green}✔ %s${reset}\n" "$@"
}
error() { printf "${red}✖ %s${reset}\n" "$@"
}
warn() { printf "${tan}➜ %s${reset}\n" "$@"
}
bold() { printf "${bold}%s${reset}\n" "$@"
}
note() { printf "\n${underline}${bold}${blue}Note:${reset} ${blue}%s${reset}\n" "$@"
}

set -e
set +o noglob



function check_docker {
	if ! docker --version &> /dev/null
	then
		error "Need to install docker(1.10.0+) first and run this script again."
		exit 1
	fi
	
	# docker has been installed and check its version
	if [[ $(docker --version) =~ (([0-9]+).([0-9]+).([0-9]+)) ]]
	then
		docker_version=${BASH_REMATCH[1]}
		docker_version_part1=${BASH_REMATCH[2]}
		docker_version_part2=${BASH_REMATCH[3]}
		
		# the version of docker does not meet the requirement
		if [ "$docker_version_part1" -lt 1 ] || ([ "$docker_version_part1" -eq 1 ] && [ "$docker_version_part2" -lt 10 ])
		then
			error "Need to upgrade docker package to 1.10.0+."
			exit 1
		else
			note "docker version: $docker_version"
		fi
	else
		error "Failed to parse docker version."
		exit 1
	fi
}


function check_dockercompose {
	if ! docker-compose --version &> /dev/null
	then
		error "Need to install docker-compose(1.7.1+) by yourself first and run this script again."
		exit 1
	fi
	
	# docker-compose has been installed, check its version
	if [[ $(docker-compose --version) =~ (([0-9]+).([0-9]+).([0-9]+)) ]]
	then
		docker_compose_version=${BASH_REMATCH[1]}
		docker_compose_version_part1=${BASH_REMATCH[2]}
		docker_compose_version_part2=${BASH_REMATCH[3]}
		
		# the version of docker-compose does not meet the requirement
		if [ "$docker_compose_version_part1" -lt 1 ] || ([ "$docker_compose_version_part1" -eq 1 ] && [ "$docker_compose_version_part2" -lt 6 ])
		then
			error "Need to upgrade docker-compose package to 1.7.1+."
                        exit 1
		else
			note "docker-compose version: $docker_compose_version"
		fi
	else
		error "Failed to parse docker-compose version."
		exit 1
	fi
}

h2 "[Step $item]: checking installation environment ..."; let item+=1
check_docker
check_dockercompose



h2 "[Step $item]: Installing Develop Environment  ..."; let item+=1 

read -p "Do you wish to install All Develop Environment (y/n)?" -r
if [[  $REPLY =~ ^[Yy]$ ]]; then


### Get IP address ###
if [[ -z "$1" ]]; then
   IPADDR=$(ip addr | grep eth | grep inet | awk '{print $2}' |cut -f1  -d'/')
fi
if [[ -z "$IPADDR" ]]; then
   IPADDR=$(ip addr | grep en | grep inet | awk '{print $2}' |cut -f1  -d'/')
fi
if [[ -z "$IPADDR" ]]; then
  read -p "找不到本機IP，請輸入IP:"     IPADDR
fi
echo "Your IP is ${IPADDR}"


sed -i 's/192.168.1.1/'"$IPADDR"'/g' all-service-traefik.yml
docker-compose -f all-service-traefik.yml up -d


success $"----All DevOps Service has been installed and started successfully.----


Now you should be able to visit the below portal .
1.Jenkins:    http://jenkins.${IPADDR}.nip.io
2.Registry:   http://registry.${IPADDR}.nip.io
3.Portainer:  http://portainer.${IPADDR}.nip.io
4.Gitlab:     http://gitlab.${IPADDR}.nip.io
5.Zentao:     http://zentao.${IPADDR}.nip.io


Now you should be able to visit the admin portal . 
For more details, please contact system administrator .
"


else
  read -p "Do you wish to install Container Managerment WebSite(Portainer) (y/n)?" -r
  if [[  $REPLY =~ ^[Yy]$ ]]; then
    ### Get IP address ###
    if [[ -z "$1" ]]; then
      IPADDR=$(ip addr | grep eth | grep inet | awk '{print $2}' |cut -f1  -d'/')
    fi
    if [[ -z "$IPADDR" ]]; then
      IPADDR=$(ip addr | grep en | grep inet | awk '{print $2}' |cut -f1  -d'/')
    fi
    if [[ -z "$IPADDR" ]]; then
      read -p "找不到本機IP，請輸入IP:"     IPADDR
    fi
      echo "Your IP is ${IPADDR}"

    sed -i 's/192.168.1.1/'"$IPADDR"'/g' portainer/docker-compose.yml
    sed -i 's/192.168.1.1/'"$IPADDR"'/g' traefik/docker-compose.yml
    sudo docker network create traefik
    sudo docker network create proxy
    sudo docker volume create portainer-data
    docker-compose -f portainer/docker-compose.yml -f traefik/docker-compose.yml up -d
    success $"---- Container Management Website (Portainer) has been installed and started successfully.----

    http://portainer.${IPADDR}.nip.io
    
    Now you should be able to visit the admin portal .
    For more details, please contact system administrator .
    "

  else
    echo "bye bye!"
    exit 1;
  fi
fi
