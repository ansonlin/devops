

kubectl create -f caddy-demo.yml

kubectl get po caddy-demo --show-labels

kubectl describe po caddy-demo

kubectl label po caddy-demo env=test

### Add node labels ###

kubectl get node

kubectl label node kube2 hardware=high-memory

kubectl label node kube2 hardware=normal --overwrite

kubectl label node kube1 hardware=high-memory

kubectl get node --show-labels

kubectl get po  -o wide
