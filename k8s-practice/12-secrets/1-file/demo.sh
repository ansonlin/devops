#!/bin/sh

echo -n 'zxcv!@#$%' > ./dbpass.txt

kubectl create secret generic db-pass-demo --from-file=dbpass.txt -n default

kubectl get secret

kubectl describe secret/db-pass-demo -n default


#### base64
echo -n 'ms' | base64 >> dbpass_base64.txt
echo -n 'zxcv!@#$%' | base64 >> dbpass_base64.txt

kubectl create -f secret-demo.yml

kubectl get secret





### decode ###
echo 'bXM=' | base64 --decode
