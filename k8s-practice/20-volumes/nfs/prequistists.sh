#!/bin/sh

## Download  k8s examples
## git clone https://github.com/kubernetes/examples.git

## Prepare for  k8s persist volumes

sudo yum install nfs-utils
sudo systemctl start rpcbind
sudo systemctl enable rpcbind


## Test for NFS
sudo mount -t nfs 10.40.140.65:/volume1/test-NFS /home/ansonlin/k8s-practice/volumes/test-NFS



## Create Persist volume
kubectl create -f nfs-pv.yml
kubectl create -f nfs-pvc.yml

## Check status
kubectl get -f nfs-pv.yaml
kubectl get pvc
