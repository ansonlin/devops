#!/bin/sh

kubectl get nodes --show-labels

kubectl label node localhost app=redis

kubectl apply -f test-redis.yml 


### show node role

kubectl get nodes 

kubectl label node localhost node-role.kubernetes.io/nodes=

kubectl get nodes

### remove label
kubectl label node localhost node-role.kubernetes.io/node-
