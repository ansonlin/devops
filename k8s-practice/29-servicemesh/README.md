1. Installation
1) 
$ cp istio-1.0.2/bin/istioctl   /usr/local/bin
$ istioctl  version


2)
$ kubectl  apply  -f  istio-1.0.2/install/kubernetes/helm/istio/templates/crds.yaml
$ kubectl  get  crd

3) 
$ kubectl  apply  -f  istio-1.0.2/install/kubernetes/istio-demo.yaml


4) Check
$ kubectl  get  svc  -n  istio-system
$ kubectl  get  pod  -n  istio-system


5) Ingress - prometheus.grafana.jaeger(Please preinstall ingress...)
$ vi  ingress-istio.yaml
$ kubectl  apply  -f   ingress-istio.yml
then Open your Web browser check it!


5) Remove - if necessary
$ kubectl  delete  -f  istio-1.0.2/install/kubernetes/istio-demo.yaml
$ kubectl  delete  -f  istio-1.0.2/install/kubernetes/helm/istio/templates/crds.yaml  -n istio-system


2. Sample
1)
$ kubectl  get  pod -n istio-system | grep  injector
$ kubectl  label  namespace default istio-injection=enabled
$ kubectl  apply  -f  istio-1.0.2/samples/bookinfo/platform/kube/bookinfo.yaml
$ kubectl  get  pod,svc
$ kubectl  get  svc -n istio-system -l app=istio-ingressgateway
$ kubectl  apply  -f  istio-1.0.2/samples/bookinfo/platform/kube/bookinfo-gateway.yaml
$ kubectl  get  gateway

2) Check
http://ip:88/productpage
