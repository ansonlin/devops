
Reference:

1.
https://ithelp.ithome.com.tw/users/20103753/ironman/1590

2.
https://ithelp.ithome.com.tw/users/20107062/ironman/1244


![prometheus] (https://kubernetes.io/images/favicon.png)


=====================================================================
Q & A

1.
訊息：ErrImagePull 與 ImagePullBackOff 兩種狀態，皆表示無法取得映像檔
查詢：kubectl get pods

2.
YAML錯誤
查詢：kubectl create -f xxx.yaml --dry-run


3.CrashLoopBackOff ==> Pod崩潰 (crash)
查詢：kubectl describe pods


4.
Services異常，無法連結
kubectl get svc
kubectl get endpoints


9.Others
kubectl get pods

kubectl describe pods

kubectl describe deployment

kubectl get events

kubectl logs

kubectl get cs

kubectl get hpa

kubectl config
...

