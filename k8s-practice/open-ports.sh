#!/bin/sh

###Master Node###
sudo systemctl start firewalld

sudo firewall-cmd --add-port 6443/tcp --permanent

sudo firewall-cmd --add-port 2379-2380/tcp --permanent

sudo firewall-cmd --add-port 10250/tcp --permanent

sudo firewall-cmd --add-port 10251/tcp --permanent

sudo firewall-cmd --add-port 10252/tcp --permanent

sudo firewall-cmd --reload

sudo systemctl enable firewalld
###Worker Node###

sudo systemctl start firewalld

sudo firewall-cmd --add-port 10250/tcp --permanent

sudo firewall-cmd --add-port 30000-32767/tcp --permanent

sudo firewall-cmd --add-port 80/tcp --permanent

sudo firewall-cmd --add-port 443/tcp --permanent

sudo firewall-cmd --reload

sudo systemctl enable firewalld
