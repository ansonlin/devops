> ## OS 
>  
>   1. Install CentOS 7.x
>   2. yum install -y open-vm-tools 
>   3. yum update -y
>   4. sudo sed -i 's/^SELINUX=.*/SELINUX=disabled/g' /etc/selinux/config
>   5. reboot
>  
>

> ## Docker  
>
> <p> curl -sSL https://get.docker.com | sh </p>
> <p> systemctl start docker && systemctl enable docker</p>
> <p> vi /etc/docker/daemon.json</p>
<pre><code>    {
    "insecure-registries": ["10.40.41.160"],
    "registry-mirrors": ["http://docker-hub.10.40.40.158.nip.io"]
    }
</code></pre>
>
> <p> systemctl restart docker</p>
> <p> docker  swarm init </p>
>
>
> ## Deploy Project    
>
>  1. Firewall Setting
>  <p> sudo firewall-cmd --add-port 9090/tcp --permanent &&  \  </p>
>  <p>sudo firewall-cmd --add-port 9093/tcp --permanent && \ </p>
>  <p>sudo firewall-cmd --add-port 3000/tcp --permanent && \ </p>
>  <p>sudo firewall-cmd --reload </p>
>  <p>sudo firewall-cmd --list-port </p>
>
>  2. Git
>  <p> sudo yum install -y git </p>
>  <p> git clone http://10.40.42.38/ansonlin/prom-monitor.git </p>
>  <p> cd prom-monitor </p>
>
>  3. Deploy
>  <p> docker stack deploy -c docker-compose monitor </p>
>
>
>
> ## Remove    
>
>  <p> docker stack rm monitor </p>
>  <p> docker system prune -a -f --volumes </p>