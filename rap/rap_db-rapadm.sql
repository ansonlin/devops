CREATE DATABASE IF NOT EXISTS rap_db
  DEFAULT CHARSET utf8
  COLLATE utf8_general_ci;

USE rap_db;


-- MySQL dump 10.13  Distrib 5.6.39, for Linux (x86_64)
--
-- Host: localhost    Database: rap_db
-- ------------------------------------------------------
-- Server version	5.6.39

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_action`
--

DROP TABLE IF EXISTS `tb_action`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_action` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `description` text,
  `request_type` int(11) NOT NULL DEFAULT '1' COMMENT '请求类型get/post/put/delete等等 request type',
  `request_url` text,
  `disable_cache` tinyint(4) NOT NULL DEFAULT '0' COMMENT '禁用Mock缓存 disable mock cache',
  `response_template` text COMMENT '响应模板地址, 暂时弃用。 response template address, temply deprecated.',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_action`
--

LOCK TABLES `tb_action` WRITE;
/*!40000 ALTER TABLE `tb_action` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_action` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_action_and_page`
--

DROP TABLE IF EXISTS `tb_action_and_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_action_and_page` (
  `action_id` int(10) NOT NULL,
  `page_id` int(10) NOT NULL,
  PRIMARY KEY (`action_id`,`page_id`),
  KEY `page_id` (`page_id`),
  CONSTRAINT `tb_action_and_page_ibfk_1` FOREIGN KEY (`action_id`) REFERENCES `tb_action` (`id`),
  CONSTRAINT `tb_action_and_page_ibfk_2` FOREIGN KEY (`page_id`) REFERENCES `tb_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_action_and_page`
--

LOCK TABLES `tb_action_and_page` WRITE;
/*!40000 ALTER TABLE `tb_action_and_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_action_and_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_check_in`
--

DROP TABLE IF EXISTS `tb_check_in`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_check_in` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tag` varchar(128) DEFAULT NULL COMMENT 'tag标签 暂时未使用 deprecated',
  `user_id` int(10) NOT NULL COMMENT '提交人 submit user id',
  `project_id` int(10) NOT NULL COMMENT '提交的项目ID submit project id',
  `description` text COMMENT '提交描述 submit description',
  `version` varchar(128) NOT NULL COMMENT '版本号 version no.',
  `project_data` longtext NOT NULL COMMENT '项目JSON数据 project json data',
  `workspace_mode` int(10) NOT NULL COMMENT '工作区模式(弃用) workspace mode(deprecated)',
  `log` text COMMENT '更新日志，用于存储与最近一个版本的对比差异。暂时未使用。update log, used for calculate versions differences. Deprecated.',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `tb_check_in_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`),
  CONSTRAINT `tb_check_in_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `tb_project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_check_in`
--

LOCK TABLES `tb_check_in` WRITE;
/*!40000 ALTER TABLE `tb_check_in` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_check_in` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_complex_parameter_list_mapping`
--

DROP TABLE IF EXISTS `tb_complex_parameter_list_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_complex_parameter_list_mapping` (
  `complex_parameter_id` int(10) NOT NULL,
  `parameter_id` int(10) NOT NULL,
  PRIMARY KEY (`complex_parameter_id`,`parameter_id`),
  KEY `parameter_id` (`parameter_id`),
  CONSTRAINT `tb_complex_parameter_list_mapping_ibfk_1` FOREIGN KEY (`complex_parameter_id`) REFERENCES `tb_parameter` (`id`),
  CONSTRAINT `tb_complex_parameter_list_mapping_ibfk_2` FOREIGN KEY (`parameter_id`) REFERENCES `tb_parameter` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_complex_parameter_list_mapping`
--

LOCK TABLES `tb_complex_parameter_list_mapping` WRITE;
/*!40000 ALTER TABLE `tb_complex_parameter_list_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_complex_parameter_list_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_corporation`
--

DROP TABLE IF EXISTS `tb_corporation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_corporation` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `logo_url` varchar(256) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `access_type` tinyint(4) NOT NULL DEFAULT '10' COMMENT '权限控制, 10普通, 20公开',
  `desc` text NOT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `tb_corporation_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_corporation`
--

LOCK TABLES `tb_corporation` WRITE;
/*!40000 ALTER TABLE `tb_corporation` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_corporation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_corporation_and_user`
--

DROP TABLE IF EXISTS `tb_corporation_and_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_corporation_and_user` (
  `user_id` int(10) NOT NULL,
  `corporation_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  PRIMARY KEY (`user_id`,`corporation_id`),
  KEY `corporation_id` (`corporation_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `tb_corporation_and_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`),
  CONSTRAINT `tb_corporation_and_user_ibfk_2` FOREIGN KEY (`corporation_id`) REFERENCES `tb_corporation` (`id`),
  CONSTRAINT `tb_corporation_and_user_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `tb_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_corporation_and_user`
--

LOCK TABLES `tb_corporation_and_user` WRITE;
/*!40000 ALTER TABLE `tb_corporation_and_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_corporation_and_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_group`
--

DROP TABLE IF EXISTS `tb_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_group` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `production_line_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `production_line_id` (`production_line_id`),
  CONSTRAINT `tb_group_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`),
  CONSTRAINT `tb_group_ibfk_2` FOREIGN KEY (`production_line_id`) REFERENCES `tb_production_line` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_group`
--

LOCK TABLES `tb_group` WRITE;
/*!40000 ALTER TABLE `tb_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_module`
--

DROP TABLE IF EXISTS `tb_module`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_module` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `project_id` int(10) NOT NULL,
  `name` varchar(256) NOT NULL,
  `introduction` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  CONSTRAINT `tb_module_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `tb_project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_module`
--

LOCK TABLES `tb_module` WRITE;
/*!40000 ALTER TABLE `tb_module` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_module` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_notification`
--

DROP TABLE IF EXISTS `tb_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_notification` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) NOT NULL COMMENT '接受通知的用户id; user id to be notified.',
  `target_user_id` int(10) NOT NULL COMMENT '上下文用户id; context user id',
  `type_id` smallint(6) NOT NULL COMMENT '1-文档修改,2-被加入新项目',
  `param1` varchar(128) DEFAULT NULL COMMENT '1,2-项目id',
  `param2` varchar(128) DEFAULT NULL COMMENT ' 1,2-项目名称',
  `param3` text COMMENT '备用预留 reserved',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间 create time',
  `is_read` smallint(6) NOT NULL DEFAULT '0' COMMENT '是否已读 is notification read',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `target_user_id` (`target_user_id`),
  CONSTRAINT `tb_notification_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`),
  CONSTRAINT `tb_notification_ibfk_2` FOREIGN KEY (`target_user_id`) REFERENCES `tb_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_notification`
--

LOCK TABLES `tb_notification` WRITE;
/*!40000 ALTER TABLE `tb_notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_page`
--

DROP TABLE IF EXISTS `tb_page`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_page` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(56) NOT NULL,
  `module_id` int(10) NOT NULL,
  `introduction` text,
  `template` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `module_id` (`module_id`),
  CONSTRAINT `tb_page_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `tb_module` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_page`
--

LOCK TABLES `tb_page` WRITE;
/*!40000 ALTER TABLE `tb_page` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_page` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_parameter`
--

DROP TABLE IF EXISTS `tb_parameter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_parameter` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) DEFAULT NULL COMMENT '参数含义 parameter name',
  `identifier` varchar(256) DEFAULT NULL COMMENT '变量名/参数标识符 parameter identifier',
  `data_type` varchar(32) DEFAULT NULL COMMENT '数据类型 data type',
  `remark` text COMMENT '备注/mock数据等 remark/mock data',
  `expression` varchar(128) DEFAULT NULL COMMENT '备用字段：表达式 backup column:expression',
  `mock_data` text COMMENT '备用字段:mock数据 backup column:mock data',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_parameter`
--

LOCK TABLES `tb_parameter` WRITE;
/*!40000 ALTER TABLE `tb_parameter` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_parameter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_production_line`
--

DROP TABLE IF EXISTS `tb_production_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_production_line` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `project_num` int(10) NOT NULL DEFAULT '0',
  `corporation_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `corporation_id` (`corporation_id`),
  CONSTRAINT `tb_production_line_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`),
  CONSTRAINT `tb_production_line_ibfk_2` FOREIGN KEY (`corporation_id`) REFERENCES `tb_corporation` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_production_line`
--

LOCK TABLES `tb_production_line` WRITE;
/*!40000 ALTER TABLE `tb_production_line` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_production_line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_project`
--

DROP TABLE IF EXISTS `tb_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_project` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `version` varchar(128) NOT NULL DEFAULT '0.0.0.1' COMMENT '版本号 version no.',
  `name` varchar(128) NOT NULL COMMENT '项目名称 project name',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期 create date',
  `user_id` int(10) NOT NULL COMMENT '创建人ID, project author id',
  `introduction` text COMMENT '项目描述 project introduction',
  `workspace_mode` int(10) NOT NULL DEFAULT '1' COMMENT '工作区提交模式(类VSS or SVN)，暂时弃用了。 Workspace submit mode, deprecated.',
  `stage` int(10) NOT NULL DEFAULT '1' COMMENT '项目阶段，暂时废弃;project stage, temply deprecated.  1-design 2-developing 3-debug',
  `project_data` longtext COMMENT '项目JSON数据，存放当前最新的版本。 project JSON data, saved the newest version of the project',
  `group_id` int(10) DEFAULT NULL COMMENT '分组ID group id',
  `related_ids` varchar(128) NOT NULL DEFAULT '' COMMENT '路由ID，用于指定与哪些项目共享mock数据; router id, used for specify sharing data with which projects.',
  `update_time` datetime NOT NULL COMMENT '更新时间 update time',
  `mock_num` int(11) NOT NULL DEFAULT '0' COMMENT 'mock次数 mock num',
  `access_type` tinyint(4) NOT NULL DEFAULT '10' COMMENT '权限控制, 10普通, 0私有',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `tb_project_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_project`
--

LOCK TABLES `tb_project` WRITE;
/*!40000 ALTER TABLE `tb_project` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_project_and_user`
--

DROP TABLE IF EXISTS `tb_project_and_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_project_and_user` (
  `project_id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `access_level` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`project_id`,`user_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `tb_project_and_user_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `tb_project` (`id`),
  CONSTRAINT `tb_project_and_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_project_and_user`
--

LOCK TABLES `tb_project_and_user` WRITE;
/*!40000 ALTER TABLE `tb_project_and_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_project_and_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_request_parameter_list_mapping`
--

DROP TABLE IF EXISTS `tb_request_parameter_list_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_request_parameter_list_mapping` (
  `action_id` int(10) NOT NULL,
  `parameter_id` int(10) NOT NULL,
  PRIMARY KEY (`action_id`,`parameter_id`),
  KEY `parameter_id` (`parameter_id`),
  CONSTRAINT `tb_request_parameter_list_mapping_ibfk_1` FOREIGN KEY (`action_id`) REFERENCES `tb_action` (`id`),
  CONSTRAINT `tb_request_parameter_list_mapping_ibfk_2` FOREIGN KEY (`parameter_id`) REFERENCES `tb_parameter` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_request_parameter_list_mapping`
--

LOCK TABLES `tb_request_parameter_list_mapping` WRITE;
/*!40000 ALTER TABLE `tb_request_parameter_list_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_request_parameter_list_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_response_parameter_list_mapping`
--

DROP TABLE IF EXISTS `tb_response_parameter_list_mapping`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_response_parameter_list_mapping` (
  `action_id` int(10) NOT NULL,
  `parameter_id` int(10) NOT NULL,
  PRIMARY KEY (`action_id`,`parameter_id`),
  KEY `parameter_id` (`parameter_id`),
  CONSTRAINT `tb_response_parameter_list_mapping_ibfk_1` FOREIGN KEY (`action_id`) REFERENCES `tb_action` (`id`),
  CONSTRAINT `tb_response_parameter_list_mapping_ibfk_2` FOREIGN KEY (`parameter_id`) REFERENCES `tb_parameter` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_response_parameter_list_mapping`
--

LOCK TABLES `tb_response_parameter_list_mapping` WRITE;
/*!40000 ALTER TABLE `tb_response_parameter_list_mapping` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_response_parameter_list_mapping` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_role`
--

DROP TABLE IF EXISTS `tb_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) NOT NULL COMMENT '角色名称 role name',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_role`
--

LOCK TABLES `tb_role` WRITE;
/*!40000 ALTER TABLE `tb_role` DISABLE KEYS */;
INSERT INTO `tb_role` VALUES (1,'god'),(2,'admin'),(3,'user');
/*!40000 ALTER TABLE `tb_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_role_and_user`
--

DROP TABLE IF EXISTS `tb_role_and_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_role_and_user` (
  `user_id` int(10) NOT NULL,
  `role_id` int(10) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `tb_role_and_user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`),
  CONSTRAINT `tb_role_and_user_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `tb_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_role_and_user`
--

LOCK TABLES `tb_role_and_user` WRITE;
/*!40000 ALTER TABLE `tb_role_and_user` DISABLE KEYS */;
INSERT INTO `tb_role_and_user` VALUES (1,1),(2,1);
/*!40000 ALTER TABLE `tb_role_and_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_rule`
--

DROP TABLE IF EXISTS `tb_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_rule` (
  `action_id` int(10) NOT NULL,
  `rules` text NOT NULL,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`action_id`),
  CONSTRAINT `tb_rule_ibfk_1` FOREIGN KEY (`action_id`) REFERENCES `tb_action` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_rule`
--

LOCK TABLES `tb_rule` WRITE;
/*!40000 ALTER TABLE `tb_rule` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user`
--

DROP TABLE IF EXISTS `tb_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `account` varchar(32) NOT NULL COMMENT '账户名 account name',
  `password` varchar(128) NOT NULL COMMENT '密码 password',
  `name` varchar(256) NOT NULL COMMENT '名字/昵称 name/nickname',
  `email` varchar(256) NOT NULL COMMENT 'email',
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期 create date',
  `is_locked_out` int(1) NOT NULL DEFAULT '0' COMMENT '用户是否锁定 is the user locked out',
  `is_hint_enabled` int(1) NOT NULL DEFAULT '1' COMMENT '是否开启新手引导 is user hint enabled',
  `last_login_date` datetime NOT NULL COMMENT '最近登录 last login date',
  `incorrect_login_attempt` int(10) NOT NULL DEFAULT '0' COMMENT '错误登录次数，登录成功后会重置为0 count of incorrect login attempts, will be set to 0 after any succesful login',
  `realname` varchar(128) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `emp_id` varchar(45) DEFAULT NULL COMMENT '工号，可选',
  `mock_num` int(10) NOT NULL DEFAULT '0' COMMENT 'mock次数，用于记录该用户所创建的接口被调用的mock次数。 mock num, used for record mock API invokation count',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user`
--

LOCK TABLES `tb_user` WRITE;
/*!40000 ALTER TABLE `tb_user` DISABLE KEYS */;
INSERT INTO `tb_user` VALUES (1,'admin','RESERVED','admin','admin@example.com','2018-03-01 07:35:01',0,1,'2018-03-01 15:35:01',0,'',NULL,0),(2,'rapadm','112fd95ca98ac29b1f216dc52e9df4cc','rapadm','rapadmin@example.com','2018-03-22 03:08:34',0,0,'2018-03-22 00:00:00',0,'',NULL,0);
/*!40000 ALTER TABLE `tb_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_user_settings`
--

DROP TABLE IF EXISTS `tb_user_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_user_settings` (
  `user_id` int(10) NOT NULL,
  `key` varchar(128) NOT NULL COMMENT '配置KEY config key',
  `value` varchar(128) NOT NULL COMMENT '配置VALUE config value',
  PRIMARY KEY (`user_id`,`key`),
  CONSTRAINT `tb_user_settings_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_user_settings`
--

LOCK TABLES `tb_user_settings` WRITE;
/*!40000 ALTER TABLE `tb_user_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_user_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_workspace`
--

DROP TABLE IF EXISTS `tb_workspace`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_workspace` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `project_id` int(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_date` datetime NOT NULL,
  `project_data` longtext NOT NULL,
  `project_data_original` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `project_id` (`project_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `tb_workspace_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `tb_project` (`id`),
  CONSTRAINT `tb_workspace_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `tb_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_workspace`
--

LOCK TABLES `tb_workspace` WRITE;
/*!40000 ALTER TABLE `tb_workspace` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_workspace` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_workspace_save`
--

DROP TABLE IF EXISTS `tb_workspace_save`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_workspace_save` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `workspace_id` int(10) NOT NULL,
  `update_date` datetime NOT NULL,
  `project_data` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `workspace_id` (`workspace_id`),
  CONSTRAINT `tb_workspace_save_ibfk_1` FOREIGN KEY (`workspace_id`) REFERENCES `tb_workspace` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_workspace_save`
--

LOCK TABLES `tb_workspace_save` WRITE;
/*!40000 ALTER TABLE `tb_workspace_save` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_workspace_save` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-22  3:13:38
